package org.nrg.structuralqc.xapi;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.nrg.action.ClientException;
import org.nrg.ccf.common.utilities.components.ScriptResult;
import org.nrg.ccf.common.utilities.constants.CommonConstants;
import org.nrg.ccf.common.utilities.pojos.ResourceZipResponseBody;
import org.nrg.ccf.common.utilities.utils.ExperimentUtils;
import org.nrg.ccf.common.utilities.utils.ResourceUtils;
import org.nrg.ccf.commons.utilities.exception.ScriptExecException;
import org.nrg.ccf.hcppipelines.pcpcomponents.client.PipelineStatusClient;
import org.nrg.ccf.hcppipelines.pcpcomponents.client.PipelineSubmissionClient;
import org.nrg.ccf.hcppipelines.pcpcomponents.contstants.PipelineConstants;
import org.nrg.ccf.hcppipelines.pcpcomponents.exception.PipelineClientException;
import org.nrg.ccf.hcppipelines.pcpcomponents.pojo.SubmissionStatus;
import org.nrg.framework.annotations.XapiRestController;
import org.nrg.structuralqc.components.ProjectSettings;
import org.nrg.structuralqc.exception.StructuralQcPluginException;
import org.nrg.structuralqc.pojos.StructuralQcSettings;
import org.nrg.structuralqc.pojos.StructuralQcStatus;
import org.nrg.structuralqc.pojos.StructuralQcZipResponseBody;
import org.nrg.structuralqc.utils.StructuralQcUtils;
import org.nrg.xapi.rest.AbstractXapiRestController;
import org.nrg.xapi.rest.XapiRequestMapping;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.model.XnatExperimentdataI;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatResourcecatalog;
import org.nrg.xdat.security.helpers.AccessLevel;
import org.nrg.xdat.security.services.RoleHolder;
import org.nrg.xdat.security.services.UserManagementServiceI;
import org.nrg.xdat.turbine.utils.AdminUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.nrg.xnat.services.archive.CatalogService;
import org.nrg.xnat.services.archive.CatalogService.Operation;
import org.nrg.xnat.web.http.ZipStreamingResponseBody;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Lazy
@XapiRestController
@Api(description = "Structural QC API")
public class StructuralQcApi extends AbstractXapiRestController {
	
	final CatalogService _catalogService;
	private ProjectSettings _projectSettings;
	
    private static final String ATTACHMENT_DISPOSITION = "attachment; filename=\"%s.%s\"";
    private static final SubmissionStatus[] IN_PROGRESS = { SubmissionStatus.RUNNING, SubmissionStatus.QUEUED, SubmissionStatus.SUBMITTED };

	@Autowired
	@Lazy
	public StructuralQcApi(UserManagementServiceI userManagementService, RoleHolder roleHolder, CatalogService catalogService,
			ProjectSettings projectSettings) {
		super(userManagementService, roleHolder);
		_catalogService = catalogService;
		_projectSettings = projectSettings;
	}
	
	@ApiOperation(value = "Run StructuralQC processing on Structural_preproc output", 
			notes = "Runs StructuralQC processing pipeline on Structural_preproc output",
			response = String.class)
    @ApiResponses({@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = {"/structuralQc/project/{projectId}/subject/{subjectId}/experiment/{experimentId}/runStructuralQcProcessing"},
    								restrictTo=AccessLevel.Read,
    								produces = {MediaType.TEXT_PLAIN_VALUE}, method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<String> runStructuralQcProcessing(@PathVariable("projectId") final String projectId,
			  @PathVariable("subjectId") final String subjectId,
			  @PathVariable("experimentId") final String experimentId
    		) throws Exception {
		try {
			final XnatExperimentdataI exp = ExperimentUtils.getExperiment(projectId, experimentId, getSessionUser());
			final XnatMrsessiondata mrSession = (exp != null && exp instanceof XnatMrsessiondata) ? (XnatMrsessiondata) exp : null;
			final StructuralQcSettings settings = _projectSettings.getStructuralQcSettings(projectId);
			if (mrSession == null) {
				throw new ClientException("ERROR:  Requested experiment is null or is not an MR Session");
			}
			final XnatResourcecatalog preprocResource = getStructuralPreprocResource(mrSession, settings);
			if (preprocResource == null) {
				throw new ClientException("ERROR:  Structural preprocessing has not been run for this session");
			}
			final ScriptResult result = StructuralQcUtils.runStructuralQcProcessing(mrSession, settings);
			final StringBuilder resourceSB = new StringBuilder("/archive/projects/").append(mrSession.getProject())
					.append("/subjects/").append(mrSession.getSubjectId()).append("/experiments/").append(mrSession.getId())
					.append("/resources/").append(settings.getStructuralPreprocResource());
			if (result.isSuccess()) {
					_catalogService.refreshResourceCatalog(getSessionUser(), resourceSB.toString(), Operation.ALL);
				return new ResponseEntity<>(result.buildResultsHtml(), HttpStatus.OK); 
			} else {
				return new ResponseEntity<>(result.buildResultsHtml(),HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} catch (Exception e) {
			log.error("Exception thrown running structural QC processing",e);
			throw e;
		}
    }
	
	
	@ApiOperation(value = "Run StructuralQC processing on hand edit output", 
			notes = "Runs StructuralQC processing pipeline on hand edit output",
			response = String.class)
    @ApiResponses({@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = {"/structuralQc/project/{projectId}/subject/{subjectId}/experiment/{experimentId}/runStructuralQcHandEditingProcessing"},
    								restrictTo=AccessLevel.Read,
    								produces = {MediaType.TEXT_PLAIN_VALUE}, method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<String> runStructuralQcHandEditingProcessing(@PathVariable("projectId") final String projectId,
			  @PathVariable("subjectId") final String subjectId,
			  @PathVariable("experimentId") final String experimentId
    		) throws Exception {
		try {
			final XnatExperimentdataI exp = ExperimentUtils.getExperiment(projectId, experimentId, getSessionUser());
			final XnatMrsessiondata mrSession = (exp != null && exp instanceof XnatMrsessiondata) ? (XnatMrsessiondata) exp : null;
			final StructuralQcSettings settings = _projectSettings.getStructuralQcSettings(projectId);
			if (mrSession == null) {
				throw new ClientException("ERROR:  Requested experiment is null or is not an MR Session");
			}
			final XnatResourcecatalog preprocResource = getStructuralPreprocResource(mrSession, settings);
			if (preprocResource == null) {
				throw new ClientException("ERROR:  Structural preprocessing has not been run for this session");
			}
			final ScriptResult result = StructuralQcUtils.runStructuralQcHandEditingProcessing(mrSession, settings);
			final StringBuilder resourceSB = new StringBuilder("/archive/projects/").append(mrSession.getProject())
					.append("/subjects/").append(mrSession.getSubjectId()).append("/experiments/").append(mrSession.getId())
					.append("/resources/").append(settings.getStructuralPreprocHandEditingResource());
			if (result.isSuccess()) {
					_catalogService.refreshResourceCatalog(getSessionUser(), resourceSB.toString(), Operation.ALL);
				return new ResponseEntity<>(result.buildResultsHtml(), HttpStatus.OK); 
			} else {
				return new ResponseEntity<>(result.buildResultsHtml(),HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} catch (Exception e) {
			log.error("Exception thrown running structural QC processing",e);
			throw e;
		}
    }
	
	
	@ApiOperation(value = "Send hand editing completion e-mail to configured e-mail addresses.", 
			notes = "Sends hand editing completion e-mail to configured e-mail addresses.",
			response = Void.class)
    @ApiResponses({@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = {"/structuralQc/project/{projectId}/subject/{subjectId}/experiment/{experimentId}/sendCompletionNotification"},
    								restrictTo=AccessLevel.Owner,
    								produces = {MediaType.TEXT_PLAIN_VALUE}, method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Void> sendCompletionNotification(@PathVariable("projectId") final String projectId,
			  @PathVariable("subjectId") final String subjectId,
			  @PathVariable("experimentId") final String experimentId
    		) throws Exception {
		try {
			final XnatExperimentdataI exp = ExperimentUtils.getExperiment(projectId, experimentId, getSessionUser());
			final XnatMrsessiondata mrSession = (exp != null && exp instanceof XnatMrsessiondata) ? (XnatMrsessiondata) exp : null;
			final StructuralQcSettings settings = _projectSettings.getStructuralQcSettings(projectId);
			if (mrSession == null) {
				throw new ClientException("ERROR:  Requested experiment is null or is not an MR Session");
			}
			final XnatResourcecatalog preprocResource = getStructuralPreprocResource(mrSession, settings);
			if (preprocResource == null) {
				throw new ClientException("ERROR:  Structural preprocessing has not been run for this session");
			}
			final String notificationListStr = _projectSettings.getNotificationList(projectId);
			if (!notificationListStr.contains("@")) {
				throw new ClientException("ERROR:  E-mail list for this project is either not configured or not valid.");
			}
			final List<String> notificationList = Arrays.asList(notificationListStr.split("[, ]+"));
			final String message = "The structural hand editing pipeline for session <b>" + mrSession.getLabel() + 
					"</b> has completed";
			final String msgSubject = XDAT.getSiteConfigPreferences().getSiteId() + 
					":  Structural hand editing pipeline complete for session " + mrSession.getLabel();
			AdminUtils.sendUserHTMLEmail(msgSubject, message, false, notificationList.toArray(new String[notificationList.size()]));
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (Exception e) {
			log.error("Exception thrown running structural QC processing",e);
			throw e;
		}
    }
	
	
	@ApiOperation(value = "Setup hand-editing", notes = "Setup hand-editing",
			response = String.class)
    @ApiResponses({@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = {"/structuralQc/project/{projectId}/subject/{subjectId}/experiment/{experimentId}/setupHandEditing"}, restrictTo=AccessLevel.Read,
    								produces = {MediaType.TEXT_PLAIN_VALUE}, method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<String> setupHandEditing(@PathVariable("projectId") final String projectId,
			  @PathVariable("subjectId") final String subjectId,
			  @PathVariable("experimentId") final String experimentId,
			  @RequestParam(value="refreshOnly", defaultValue="false") final Boolean refreshOnly
    		) throws Exception {
		final XnatExperimentdataI exp = ExperimentUtils.getExperiment(projectId, experimentId, getSessionUser());
		if (exp == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND); 
		}
		final XnatMrsessiondata mrSession = (exp instanceof XnatMrsessiondata) ? (XnatMrsessiondata) exp : null;
		if (mrSession == null) {
			return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY); 
		}
		final StructuralQcSettings settings = _projectSettings.getStructuralQcSettings(projectId);

		try {
			final XnatResourcecatalog handeditingResource = StructuralQcUtils.setupHandEditing(mrSession, settings, getSessionUser(), refreshOnly);
			final StringBuilder resourceSB = new StringBuilder("/archive/projects/").append(mrSession.getProject())
					.append("/subjects/").append(mrSession.getSubjectId()).append("/experiments/").append(mrSession.getId())
					.append("/resources/").append(handeditingResource.getLabel());
			_catalogService.refreshResourceCatalog(getSessionUser(), resourceSB.toString(), Operation.ALL);
			return new ResponseEntity<>("Generation of hand editing resource is complete", HttpStatus.OK); 
		//} catch (StructuralQcPluginException e) {
		} catch (Exception e) {
			log.error("StructuralQcPluginException:", e);
			return new ResponseEntity<>(ExceptionUtils.getFullStackTrace(e), HttpStatus.INTERNAL_SERVER_ERROR); 
		}
    }
	
    @ApiOperation(value = "Downloads a zip of of hand-editing files",
                  response = StreamingResponseBody.class)
    @ApiResponses({@ApiResponse(code = 200, message = "The requested resources were successfully downloaded."),
                   @ApiResponse(code = 204, message = "No resources were specified."),
                   @ApiResponse(code = 400, message = "Something is wrong with the request format."),
                   @ApiResponse(code = 403, message = "The user is not authorized to access one or more of the specified resources."),
                   @ApiResponse(code = 404, message = "The request was valid but one or more of the specified resources was not found."),
                   @ApiResponse(code = 500, message = "An unexpected or unknown error occurred")})
    @XapiRequestMapping(value = {"/handEditingFiles/project/{projectId}/subject/{subjectId}/experiment/{experimentId}/download"}, 
    		produces = ZipStreamingResponseBody.MEDIA_TYPE, restrictTo=AccessLevel.Read, method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<StreamingResponseBody> downloadHandEditing(@PathVariable("projectId") final String projectId,
			  @PathVariable("subjectId") final String subjectId,
			  @PathVariable("experimentId") final String experimentId
    		) throws Exception {
		final XnatExperimentdataI exp = ExperimentUtils.getExperiment(projectId, experimentId, getSessionUser());
		final XnatMrsessiondata mrSession = (exp != null && exp instanceof XnatMrsessiondata) ? (XnatMrsessiondata) exp : null;
		if (mrSession == null) {
			throw new ClientException("ERROR:  Requested experiment is null or is not an MR Session");
		}
		final StructuralQcSettings settings = _projectSettings.getStructuralQcSettings(projectId);
		final XnatResourcecatalog handEditingResource = StructuralQcUtils.getHandEditingResource(mrSession, settings, getSessionUser());
		if (handEditingResource == null) {
			throw new ClientException("ERROR:  A hand-editing resource does not exist for this session.");
		}
		final String attachmentName = mrSession.getLabel() + "_HandEditingFiles";
        return ResponseEntity.ok()
                             .header(HttpHeaders.CONTENT_TYPE, ZipStreamingResponseBody.MEDIA_TYPE)
                             .header(HttpHeaders.CONTENT_DISPOSITION, getAttachmentDisposition(attachmentName, "zip") + ";")
                             .body((StreamingResponseBody) new ResourceZipResponseBody(handEditingResource));
    }
	
	
	
    @ApiOperation(value = "Downloads a zip of of StructuralQC results and referenced files",
                  response = StreamingResponseBody.class)
    @ApiResponses({@ApiResponse(code = 200, message = "The requested resources were successfully downloaded."),
                   @ApiResponse(code = 204, message = "No resources were specified."),
                   @ApiResponse(code = 400, message = "Something is wrong with the request format."),
                   @ApiResponse(code = 403, message = "The user is not authorized to access one or more of the specified resources."),
                   @ApiResponse(code = 404, message = "The request was valid but one or more of the specified resources was not found."),
                   @ApiResponse(code = 500, message = "An unexpected or unknown error occurred")})
    @XapiRequestMapping(value = {"/structuralQc/project/{projectId}/subject/{subjectId}/experiment/{experimentId}/download"}, 
    		produces = ZipStreamingResponseBody.MEDIA_TYPE, restrictTo=AccessLevel.Read, method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<StreamingResponseBody> downloadZip(@PathVariable("projectId") final String projectId,
			  @PathVariable("subjectId") final String subjectId,
			  @PathVariable("experimentId") final String experimentId
    		) throws Exception {
		final XnatExperimentdataI exp = ExperimentUtils.getExperiment(projectId, experimentId, getSessionUser());
		final XnatMrsessiondata mrSession = (exp != null && exp instanceof XnatMrsessiondata) ? (XnatMrsessiondata) exp : null;
		final StructuralQcSettings settings = _projectSettings.getStructuralQcSettings(projectId);
		if (mrSession == null) {
			throw new ClientException("ERROR:  Requested experiment is null or is not an MR Session");
		}
		final XnatResourcecatalog preprocResource = getStructuralPreprocResource(mrSession, settings);
		if (preprocResource == null) {
			throw new ClientException("ERROR:  Structural preprocessing has not been run for this session");
		}
		if (!StructuralQcUtils.hasStructuralQcOutput(preprocResource)) {
			throw new ClientException("ERROR:  StructuralQC has not been run for this session");
		}
		final String attachmentName = mrSession.getLabel() + "_StructuralQC";
        return ResponseEntity.ok()
                             .header(HttpHeaders.CONTENT_TYPE, ZipStreamingResponseBody.MEDIA_TYPE)
                             .header(HttpHeaders.CONTENT_DISPOSITION, getAttachmentDisposition(attachmentName, "zip") + ";")
                             .body((StreamingResponseBody) new StructuralQcZipResponseBody(preprocResource));
    }
	
    @ApiOperation(value = "Structural QC status",
                  response = StructuralQcStatus.class)
    @ApiResponses({@ApiResponse(code = 200, message = "OK"),
                   @ApiResponse(code = 404, message = "The requested experiment does not exist"),
                   @ApiResponse(code = 500, message = "An unexpected or unknown error occurred")})
    @XapiRequestMapping(value = {"/structuralQc/project/{projectId}/subject/{subjectId}/experiment/{experimentId}/status"}, 
    		produces = MediaType.APPLICATION_JSON_VALUE, restrictTo=AccessLevel.Read, method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<StructuralQcStatus> getStatus(@PathVariable("projectId") final String projectId,
			  @PathVariable("subjectId") final String subjectId,
			  @PathVariable("experimentId") final String experimentId
    		) throws Exception {
		final XnatExperimentdataI exp = ExperimentUtils.getExperiment(projectId, experimentId, getSessionUser());
		final XnatMrsessiondata mrSession = (exp != null && exp instanceof XnatMrsessiondata) ? (XnatMrsessiondata) exp : null;
		final StructuralQcSettings settings = _projectSettings.getStructuralQcSettings(projectId);
		final StructuralQcStatus status = new StructuralQcStatus();
		if (mrSession == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		status.setStructuralQcEnabled(_projectSettings.getStructuralQcEnabled(projectId));
		final XnatResourcecatalog preprocResource = getStructuralPreprocResource(mrSession, settings);
		final XnatResourcecatalog handeditResource = ResourceUtils.getResource(mrSession, PipelineConstants.STRUC_HANDEDIT_RESOURCE_LABEL);
		if (preprocResource != null) {
			status.setHasPreprocOutput(true);
			if (StructuralQcUtils.hasStructuralQcOutput(preprocResource)) {
				status.setHasStructuralQcOutput(true);
			}
			if (StructuralQcUtils.hasHandEditingSetup(mrSession, settings)) {
				status.setHasHandEditingSetup(true);
			}
			if (ResourceUtils.hasResource(mrSession, PipelineConstants.STRUC_HANDEDIT_RESOURCE_LABEL)) {
				status.setHasRerunResults(true);
				if (preprocResource.getCatalogFile(CommonConstants.ROOT_PATH).lastModified() >= 
						handeditResource.getCatalogFile(CommonConstants.ROOT_PATH).lastModified()) {
					status.setAlreadyReplaced(true);
				}
			}
		}
		return new ResponseEntity<>(status, HttpStatus.OK);
    }
	
	@ApiOperation(value = "Launch structural preprocessing hand-editing pipeline", 
			notes = "Launch structural preprocessing hand-editing pipeline",
			response = String.class)
    @ApiResponses({@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = {"/structuralQc/project/{projectId}/subject/{subjectId}/experiment/{experimentId}/launchHandEditingPreprocPipeline"},
    restrictTo=AccessLevel.Member, produces = {MediaType.TEXT_PLAIN_VALUE}, method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<String> launchHandEditing(@PathVariable("projectId") final String projectId,
			  @PathVariable("subjectId") final String subjectId,
			  @PathVariable("experimentId") final String experimentId,
			  @RequestBody final String reconStagesToRun
    		) {
		final XnatExperimentdataI exp = ExperimentUtils.getExperiment(projectId, experimentId, getSessionUser());
		if (exp == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND); 
		}
		final XnatMrsessiondata mrSession = (exp instanceof XnatMrsessiondata) ? (XnatMrsessiondata) exp : null;
		if (mrSession == null) {
			return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY); 
		}
		final String sessionLabel = mrSession.getLabel();
		final String subject_id = sessionLabel.replaceFirst("_.*$", "");
		final String classifier = sessionLabel.replaceFirst("^[^_]*_", "");
		final String project = mrSession.getProject();
		
		final SubmissionStatus jobStatus = PipelineStatusClient.getJobStatus(sessionLabel);
		if ( Arrays.asList(IN_PROGRESS).contains(jobStatus) ) {
			return new ResponseEntity<>("ERROR:  Pipeline is already running for this session (SESSION=" + 
					sessionLabel + ")", HttpStatus.CONFLICT);
		}
		final ScriptResult result;
		try {
			result = PipelineSubmissionClient.submitPipeline(subject_id, classifier, "all", project, PipelineConstants.STRUC_HANDEDIT_PIPELINE_NAME, reconStagesToRun);
		} catch (Exception e) {
			return new ResponseEntity<>("Could not submit pipeline (EXCEPTION=" + e.toString() + ")", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		if (result == null) {
			return new ResponseEntity<>("Could not submit pipeline (RESULT=NULL)", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		if (result.isSuccess()) {
			return new ResponseEntity<>("Successfully submitted.  You will receive an e-mail upon completion.", HttpStatus.OK); 
		} else {
			return new ResponseEntity<>("ERROR:  Could not submit pipeline<br>" +
					 "      <li>STATUS:  " + result.getStatus() + "</li>" +
					 "      <li>STDOUT:  " + result.getStdout().toString() + "</li>" +
					 "      <li>STDERR:  " + result.getStderr().toString() + "</li>" 
					, HttpStatus.INTERNAL_SERVER_ERROR);
		}
    }
	
	@ApiOperation(value = "Replace struc preproc output with hand edited reprocessing", 
			notes = "Replace structural preprocessing output with hand edited reprocessing", 
			response = String.class)
    @ApiResponses({@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = {"/structuralQc/project/{projectId}/subject/{subjectId}/experiment/{experimentId}/replaceHandEditingOutput"},
    restrictTo=AccessLevel.Owner, produces = {MediaType.TEXT_PLAIN_VALUE}, method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<String> replaceHandEditing(@PathVariable("projectId") final String projectId,
			  @PathVariable("subjectId") final String subjectId,
			  @PathVariable("experimentId") final String experimentId
    		) {
		final XnatExperimentdataI exp = ExperimentUtils.getExperiment(projectId, experimentId, getSessionUser());
		if (exp == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND); 
		}
		final XnatMrsessiondata mrSession = (exp instanceof XnatMrsessiondata) ? (XnatMrsessiondata) exp : null;
		if (mrSession == null) {
			return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY); 
		}
		if (!ResourceUtils.hasResource(mrSession, PipelineConstants.STRUC_PREPROC_RESOURCE_LABEL)) {
			return new ResponseEntity<>("ERROR:  Session does not have structural preprocessing output", HttpStatus.BAD_REQUEST);
		}
		if (!ResourceUtils.hasResource(mrSession, PipelineConstants.STRUC_HANDEDIT_RESOURCE_LABEL)) {
			return new ResponseEntity<>("ERROR:  Session does not have structural preprocessing hand editing output", HttpStatus.BAD_REQUEST);
		}
		try {
			if (StructuralQcUtils.replaceHandEditingOutput(mrSession, _catalogService, getSessionUser())) {
				return new ResponseEntity<>(HttpStatus.OK); 
			} else {
				return new ResponseEntity<>("ERROR:  Could not replace output.", HttpStatus.INTERNAL_SERVER_ERROR) ; 
			}
		} catch (StructuralQcPluginException e) {
			return new ResponseEntity<>("ERROR:  Could not replace output (EXCEPTION=" + e.toString() + ")",
					HttpStatus.INTERNAL_SERVER_ERROR) ; 
		}
    }
	
	@ApiOperation(value = "Check pipeline processing status for session", 
			notes = "Check pipeline processing status for session", 
			response = String.class)
    @ApiResponses({@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = {"/structuralQc/project/{projectId}/subject/{subjectId}/experiment/{experimentId}/checkProcessingStatus"},
    restrictTo=AccessLevel.Member, produces = {MediaType.TEXT_PLAIN_VALUE}, method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<String> checkProcessingStatus(@PathVariable("projectId") final String projectId,
			  @PathVariable("subjectId") final String subjectId,
			  @PathVariable("experimentId") final String experimentId
    		) {
		
		
		
		final XnatExperimentdataI exp = ExperimentUtils.getExperiment(projectId, experimentId, getSessionUser());
		if (exp == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND); 
		}
		final XnatMrsessiondata mrSession = (exp instanceof XnatMrsessiondata) ? (XnatMrsessiondata) exp : null;
		if (mrSession == null) {
			return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY); 
		}
		return new ResponseEntity<>(PipelineStatusClient.getDetailedJobStatus(exp.getLabel()), HttpStatus.OK);
    }
    

	public static XnatResourcecatalog getStructuralPreprocResource(final XnatExperimentdataI exp, final StructuralQcSettings settings)  {
		return ResourceUtils.getResource(exp, settings.getStructuralPreprocResource());
	}
 
	private static String getAttachmentDisposition(final String name, final String extension) {
        return String.format(ATTACHMENT_DISPOSITION, name, extension);
    }

}
