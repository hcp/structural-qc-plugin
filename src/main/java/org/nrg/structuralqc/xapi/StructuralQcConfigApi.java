package org.nrg.structuralqc.xapi;

import org.nrg.framework.annotations.XapiRestController;
import org.nrg.framework.exceptions.NrgServiceException;
import org.nrg.structuralqc.components.ProjectSettings;
import org.nrg.structuralqc.components.SiteSettings;
import org.nrg.structuralqc.components.StructuralQcInitializer;
import org.nrg.structuralqc.pojos.StructuralQcSettings;
import org.nrg.xapi.rest.AbstractXapiRestController;
import org.nrg.xapi.rest.ProjectId;
import org.nrg.xapi.rest.XapiRequestMapping;
import org.nrg.xdat.security.helpers.AccessLevel;
import org.nrg.xdat.security.services.RoleHolder;
import org.nrg.xdat.security.services.UserManagementServiceI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
//import lombok.extern.slf4j.Slf4j;

//@Slf4j
@Lazy
@XapiRestController
@Api(description = "Structural QC API")
public class StructuralQcConfigApi extends AbstractXapiRestController {

	private SiteSettings _siteSettings;
	private ProjectSettings _projectSettings;
	private StructuralQcInitializer _initializer;

	@Autowired
	@Lazy
	public StructuralQcConfigApi(UserManagementServiceI userManagementService, RoleHolder roleHolder, SiteSettings siteSettings,
			ProjectSettings projectSettings, StructuralQcInitializer initializer) {
		super(userManagementService, roleHolder);
		_siteSettings = siteSettings;
		_projectSettings = projectSettings;
		_initializer = initializer;
	}
	
	@ApiOperation(value = "Gets site structural qc settings", notes = "Returns site-level structural qc settings",
			response = StructuralQcSettings.class)
    @ApiResponses({@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = {"/structuralQcConfig/settings"}, restrictTo=AccessLevel.Authenticated, produces = {MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<StructuralQcSettings> getStructuralQcSettings() throws NrgServiceException {
		
		return new ResponseEntity<StructuralQcSettings>(_siteSettings.getStructuralQcSettings(),HttpStatus.OK);
	
    }
	
	@ApiOperation(value = "Sets site structural qc settings", notes = "Sets site-level structural qc settings",
			response = Void.class)
    @ApiResponses({@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = {"/structuralQcConfig/settings"}, restrictTo=AccessLevel.Admin, method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Void> setStructuralQcSettings(@RequestBody final StructuralQcSettings settings) throws NrgServiceException {
		
		_siteSettings.setStructuralQcScript(settings.getStructuralQcScript());
		_siteSettings.setTemplatesFolder(settings.getTemplatesFolder());
		_siteSettings.setTemplateType(settings.getTemplateType());
		_siteSettings.setWbCommandPath(settings.getWbCommandPath());
		_siteSettings.setFslCommandPath(settings.getFslCommandPath());
		_siteSettings.setStructuralPreprocResource(settings.getStructuralPreprocResource());
		_siteSettings.setHandEditingResource(settings.getHandEditingResource());
		_siteSettings.setStructuralPreprocHandEditingResource(settings.getStructuralPreprocHandEditingResource());
		_siteSettings.setNotificationList(settings.getNotificationList());
		return new ResponseEntity<>(HttpStatus.OK);
	
    }
	
	@ApiOperation(value = "Gets project structural qc settings", notes = "Returns NDA structural qc plugin project-level settings.",
			response = StructuralQcSettings.class)
    @ApiResponses({@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = {"/structuralQcConfig/{projectId}/settings"}, restrictTo=AccessLevel.Read, produces = {MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<StructuralQcSettings> getStructuralQcSettings(@PathVariable("projectId") @ProjectId final String projectId) throws NrgServiceException {
		
		StructuralQcSettings structuralQcSettings = _projectSettings.getStructuralQcSettings(projectId); 
		if ((structuralQcSettings.getStructuralQcScript() == null || structuralQcSettings.getStructuralQcScript().isEmpty()) && 
				(structuralQcSettings.getStructuralPreprocResource() == null || structuralQcSettings.getStructuralPreprocResource().isEmpty())) {
			structuralQcSettings = _siteSettings.getStructuralQcSettings();
			structuralQcSettings.setStructuralQcEnabled(false);
			_projectSettings.setStructuralQcEnabled(projectId, structuralQcSettings.getStructuralQcEnabled());
			_projectSettings.setStructuralQcScript(projectId, structuralQcSettings.getStructuralQcScript());
			_projectSettings.setTemplatesFolder(projectId, structuralQcSettings.getTemplatesFolder());
			_projectSettings.setTemplateType(projectId, structuralQcSettings.getTemplateType());
			_projectSettings.setWbCommandPath(projectId, structuralQcSettings.getWbCommandPath());
			_projectSettings.setFslCommandPath(projectId, structuralQcSettings.getFslCommandPath());
			_projectSettings.setStructuralPreprocResource(projectId, structuralQcSettings.getStructuralPreprocResource());
			_projectSettings.setHandEditingResource(projectId, structuralQcSettings.getHandEditingResource());
			_projectSettings.setStructuralPreprocHandEditingResource(projectId, structuralQcSettings.getStructuralPreprocHandEditingResource());
			_projectSettings.setNotificationList(projectId, structuralQcSettings.getNotificationList());
		}
		return new ResponseEntity<StructuralQcSettings>(structuralQcSettings,HttpStatus.OK);
		
    }
	
	@ApiOperation(value = "Sets project transfer settings", notes = "Sets project-level transfer settings",
			response = Void.class)
    @ApiResponses({@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = {"/structuralQcConfig/{projectId}/settings"}, restrictTo=AccessLevel.Owner, method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Void> setStructuralQcSettings(@PathVariable("projectId") @ProjectId final String projectId, @RequestBody final StructuralQcSettings settings) throws NrgServiceException {
		if (settings.getStructuralQcEnabled()) {
			_initializer.initializeStructuralQc(projectId);
		}
		_projectSettings.setStructuralQcEnabled(projectId, settings.getStructuralQcEnabled());
		_projectSettings.setStructuralQcScript(projectId, settings.getStructuralQcScript());
		_projectSettings.setTemplatesFolder(projectId, settings.getTemplatesFolder());
		_projectSettings.setTemplateType(projectId, settings.getTemplateType());
		_projectSettings.setWbCommandPath(projectId, settings.getWbCommandPath());
		_projectSettings.setFslCommandPath(projectId, settings.getFslCommandPath());
		_projectSettings.setStructuralPreprocResource(projectId, settings.getStructuralPreprocResource());
		_projectSettings.setHandEditingResource(projectId, settings.getHandEditingResource());
		_projectSettings.setStructuralPreprocHandEditingResource(projectId, settings.getStructuralPreprocHandEditingResource());
		_projectSettings.setNotificationList(projectId, settings.getNotificationList());
		return new ResponseEntity<>(HttpStatus.OK);
	
    }
	
	
}
