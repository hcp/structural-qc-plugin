package org.nrg.structuralqc.event.listeners;

import reactor.bus.Event;
import reactor.bus.EventBus;
import reactor.fn.Consumer;

import org.nrg.ccf.common.utilities.components.EmailUtils;
import org.nrg.ccf.common.utilities.constants.CommonConstants;
import org.nrg.ccf.common.utilities.utils.ResourceUtils;
import org.nrg.ccf.hcppipelines.pcpcomponents.contstants.PipelineConstants;
import org.nrg.structuralqc.components.ProjectSettings;
import org.nrg.structuralqc.pojos.StructuralQcSettings;
import org.nrg.structuralqc.utils.StructuralQcUtils;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.om.WrkWorkflowdata;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatResourcecatalog;
import org.nrg.xdat.turbine.utils.AdminUtils;
import org.nrg.xft.event.entities.WorkflowStatusEvent;
import org.nrg.xft.event.persist.PersistentWorkflowUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Lazy;
//import org.springframework.stereotype.Service;
import static reactor.bus.selector.Selectors.R;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

//
// NOTE:  This service/code is not currently in use but is being left in place in case we want to use it in the future //
//        Currently, we are using the REST API called on the CHPC side during the PUT_DATA step to send the e-mail     //
//

//@Service
//@Lazy
public class PipelineCompletionNotifier implements Consumer<Event<WorkflowStatusEvent>> {
	
	final static Map<String,Long> _monitoring = new HashMap<>();
	private ProjectSettings _projectSettings;
	private List<String> _disabledUserEmailList = new ArrayList<>();
	
	public void monitorForCompletion(String session) {
		final long currentTime = System.currentTimeMillis();
		_monitoring.put(session, currentTime);
		// Remove old entries
		for (Entry<String, Long> entry : _monitoring.entrySet()) {
			if ((currentTime - entry.getValue())>(1000*60*60*24*3)) {
				_monitoring.remove(entry.getKey());
			}
		}
	}
	
    //@Autowired
    public PipelineCompletionNotifier(final EventBus eventBus, ProjectSettings projectSettings, EmailUtils emailUtils) {
		eventBus.on(R(WorkflowStatusEvent.class.getName() + "[.]?(" + PersistentWorkflowUtils.COMPLETE + ")"), this);
		_projectSettings = projectSettings;
		_disabledUserEmailList.addAll(emailUtils.getDisabledUserEmailList());
    }

    @Override
    public void accept(final Event<WorkflowStatusEvent> event) {

        final WorkflowStatusEvent wfsEvent = event.getData();
        if (wfsEvent.getWorkflow() instanceof WrkWorkflowdata) {
            handleEvent(wfsEvent);
        }

    }

	private void handleEvent(final WorkflowStatusEvent wfsEvent) {
		
		final WrkWorkflowdata workflow = (WrkWorkflowdata)wfsEvent.getWorkflow();
		final String id = workflow.getId();
		if (_monitoring.containsKey(id)) {
			final String notificationListStr = _projectSettings.getNotificationList(workflow.getExternalid());
			if (!notificationListStr.contains("@")) {
				return;
			}
			final List<String> notificationList = Arrays.asList(notificationListStr.split("[, ]+"));
			Iterator<String> i = notificationList.iterator();
			while (i.hasNext()) {
				final String userEmail = i.next();
				if (_disabledUserEmailList.contains(userEmail) || !userEmail.contains("@")) {
					i.remove();
				}
			}
			final Long submitted = _monitoring.get(id);
			final XnatImagesessiondata session = XnatImagesessiondata.getXnatImagesessiondatasById(id, workflow.getUser(), false);
			if (session == null) {
				return;
			}
			if (session instanceof XnatMrsessiondata) {
				// Run StructuralQC on Structural_preproc_handedit resource
				final StructuralQcSettings settings = _projectSettings.getStructuralQcSettings(workflow.getExternalid());
				StructuralQcUtils.runStructuralQcHandEditingProcessing((XnatMrsessiondata)session, settings);
			}
			final XnatResourcecatalog handeditResource = ResourceUtils.getResource(session, PipelineConstants.STRUC_HANDEDIT_RESOURCE_LABEL);
			if (handeditResource == null) {
				return;
			}
			
			final File catalogFile = handeditResource.getCatalogFile(CommonConstants.ROOT_PATH);
			if (catalogFile == null) {
				return;
			}
			if (catalogFile.lastModified() < submitted) {
				return;
			}
			if (notificationList.size()<1) {
				return;
			}
			_monitoring.remove(id);
			final String message = "The structural hand editing pipeline for session <b>" + session.getLabel() + 
					"</b> has completed";
			final String msgSubject = XDAT.getSiteConfigPreferences().getSiteId() + 
					":  Structural hand editing pipeline complete for session " + session.getLabel();
			AdminUtils.sendUserHTMLEmail(msgSubject, message, false, notificationList.toArray(new String[notificationList.size()]));
		}
		
	}

}
