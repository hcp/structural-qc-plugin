package org.nrg.structuralqc.components;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.nrg.automation.entities.EventFilters;
import org.nrg.automation.entities.Script;
import org.nrg.automation.entities.ScriptTrigger;
import org.nrg.automation.services.ScriptRunnerService;
import org.nrg.automation.services.ScriptTriggerService;
import org.nrg.framework.constants.Scope;
import org.nrg.framework.utilities.BasicXnatResourceLocator;
import org.nrg.xdat.security.helpers.Users;
import org.nrg.xft.event.EventDetails;
import org.nrg.xft.event.EventUtils;
import org.nrg.xft.event.persist.PersistentWorkflowI;
import org.nrg.xft.event.persist.PersistentWorkflowUtils;
import org.nrg.xnat.event.entities.ScriptLaunchRequestEvent;
import org.nrg.xnat.utils.WorkflowUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class StructuralQcInitializer {
	
	private static String STRUCTQC_HANDEDIT_SCRIPTID = "StructQcHandEditFileUpload";
	private static String STRUCTQC_HANDEDIT_SCRIPTLABEL = "StructQC Hand Editing File Upload";
	private static String STRUCTQC_HANDEDIT_SCRIPTDESC = "Structural QC Hand Editing File Upload";
	private static String STRUCTQC_HANDEDIT_EVENT = "UploadStructQcHandEditing";
	private static String STRUCTQC_HANDEDIT_EVENTDESC = "Upload StructQc Hand Editing Files"; 
	private ScriptRunnerService _scriptService;
	private ScriptTriggerService _scriptTriggerService;
	
	@Autowired
	public StructuralQcInitializer(ScriptRunnerService scriptService, ScriptTriggerService scriptTriggerService) {
		super();
		this._scriptService = scriptService;
		this._scriptTriggerService = scriptTriggerService;
	}

	public void initializeStructuralQc(final String projectId) {
		if (initializeScript()) {
			initializeEventHandler(projectId);
		}
	}

	private boolean initializeScript() {
		try {
			final List<Resource> resList = BasicXnatResourceLocator.getResources("classpath*:META-INF/resources/structuralQc/HandEditingImportScript.txt");
			String newContent = null;
			if (resList.size()>0) {
				InputStream handEditingStream = resList.get(0).getInputStream();
				try {
					newContent = IOUtils.toString(handEditingStream, StandardCharsets.UTF_8.name());
				} finally {
					handEditingStream.close();
				}
			}
			final Script script = _scriptService.getScript(STRUCTQC_HANDEDIT_SCRIPTID);
			if (script != null && newContent != null) {
				if (!script.getContent().equals(newContent)) {
					_scriptService.setScript(STRUCTQC_HANDEDIT_SCRIPTID, STRUCTQC_HANDEDIT_SCRIPTLABEL, newContent, STRUCTQC_HANDEDIT_SCRIPTDESC);
					recordAutomationEvent(STRUCTQC_HANDEDIT_SCRIPTID, Scope.Site.toString(), "Update", Script.class);
				} 
				return true;
			} else if (newContent != null) {
				_scriptService.setScript(STRUCTQC_HANDEDIT_SCRIPTID, STRUCTQC_HANDEDIT_SCRIPTLABEL, newContent, STRUCTQC_HANDEDIT_SCRIPTDESC);
				recordAutomationEvent(STRUCTQC_HANDEDIT_SCRIPTID, Scope.Site.toString(), "Add", Script.class);
				return true;
			} else if (script != null) {
				return true;
			}
		} catch (IOException e) {
			log.error("StructuralQcInitializer threw exception:  " + e);
		}
		return false;
	}

	private void initializeEventHandler(final String projectId) {
		List<ScriptTrigger> eventHandlers = _scriptTriggerService.getByScriptId(STRUCTQC_HANDEDIT_SCRIPTID);
		for (final ScriptTrigger eventHandler : eventHandlers) {
			if (eventHandler.getAssociation().equals("prj:" + projectId)) {
				// Already initialized
				return;
			}
		}
		final HashMap<String, List<String>> eventFiltersMap = new HashMap<String,List<String>>();
		final Set<EventFilters> eventFilters = new HashSet<EventFilters>();
		final String triggerId = _scriptTriggerService.getDefaultTriggerName(STRUCTQC_HANDEDIT_SCRIPTID, Scope.Project, projectId,
				ScriptLaunchRequestEvent.class.getCanonicalName(), STRUCTQC_HANDEDIT_EVENT, eventFiltersMap);
		final ScriptTrigger trigger = new ScriptTrigger();
		trigger.setEnabled(true);
		trigger.setTimestamp(new Date());
		trigger.setAssociation("prj:" + projectId);
		trigger.setDescription(STRUCTQC_HANDEDIT_EVENTDESC);
		trigger.setEvent(STRUCTQC_HANDEDIT_EVENT);
		trigger.setScriptId(STRUCTQC_HANDEDIT_SCRIPTID);
		trigger.setSrcEventClass(ScriptLaunchRequestEvent.class.getCanonicalName());
		trigger.setTriggerId(triggerId);
		trigger.setEventFilters(eventFilters);
		_scriptTriggerService.create(trigger);
	}
	
	// NOTE:  This method is lifted from XNAT's SecureResource class
    protected void recordAutomationEvent(final String automationId, final String containerId, final String operation, final Class<?> type) {
        try {
            final EventDetails instance = EventUtils.newEventInstance(EventUtils.CATEGORY.DATA, EventUtils.TYPE.WEB_SERVICE, operation, "", operation + " " + type + " with ID " + automationId);
            PersistentWorkflowI workflow = PersistentWorkflowUtils.buildOpenWorkflow(Users.getUser("admin"), type.getName(), automationId, containerId, instance);
            assert workflow != null;
            workflow.setStatus(PersistentWorkflowUtils.COMPLETE);
            WorkflowUtils.save(workflow, workflow.buildEvent());
        } catch (PersistentWorkflowUtils.ActionNameAbsent | PersistentWorkflowUtils.IDAbsent | PersistentWorkflowUtils.JustificationAbsent exception) {
            // This is not really going to happen because we're providing all the attributes required, but we still have to handle it.
            log.warn("An error occurred trying to save a workflow when working with event", exception);
        } catch (Exception exception) {
            log.error("An error occurred trying to save a workflow when working with event", exception);
        }
    }

}
