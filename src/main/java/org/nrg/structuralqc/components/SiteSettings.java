package org.nrg.structuralqc.components;

import org.nrg.prefs.annotations.NrgPreference;
import org.nrg.prefs.annotations.NrgPreferenceBean;
import org.nrg.prefs.beans.AbstractPreferenceBean;
import org.nrg.prefs.exceptions.InvalidPreferenceName;
import org.nrg.prefs.services.NrgPreferenceService;
import org.nrg.structuralqc.pojos.StructuralQcSettings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
@NrgPreferenceBean(toolId = "structuralQcSiteSettings", toolName = "Structural QC Site Settings")
public class SiteSettings extends AbstractPreferenceBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1502863650208146381L;

	@Autowired
	protected SiteSettings(NrgPreferenceService preferenceService) {
		super(preferenceService);
	}

	@NrgPreference
	public String getStructuralQcScript() {
		return this.getValue("structuralQcScript");
	}

	public void setStructuralQcScript(final String structuralQcScript) {
		try {
			this.set(structuralQcScript, "structuralQcScript");
		} catch (InvalidPreferenceName invalidPreferenceName) {
			log.error("Invalid preference Name", invalidPreferenceName);
		}
	}

	@NrgPreference
	public String getTemplatesFolder() {
		return this.getValue("templatesFolder");
	}

	public void setTemplatesFolder(final String templatesFolder) {
		try {
			this.set(templatesFolder, "templatesFolder");
		} catch (InvalidPreferenceName invalidPreferenceName) {
			log.error("Invalid preference Name", invalidPreferenceName);
		}
	}

	@NrgPreference
	public String getTemplateType() {
		return this.getValue("templateType");
	}

	public void setTemplateType(final String templateType) {
		try {
			this.set(templateType, "templateType");
		} catch (InvalidPreferenceName invalidPreferenceName) {
			log.error("Invalid preference Name", invalidPreferenceName);
		}
	}

	@NrgPreference
	public String getWbCommandPath() {
		return this.getValue("wbCommandPath");
	}

	public void setWbCommandPath(final String wbCommandPath) {
		try {
			this.set(wbCommandPath, "wbCommandPath");
		} catch (InvalidPreferenceName invalidPreferenceName) {
			log.error("Invalid preference Name", invalidPreferenceName);
		}
	}

	@NrgPreference
	public String getFslCommandPath() {
		return this.getValue("fslCommandPath");
	}

	public void setFslCommandPath(final String fslCommandPath) {
		try {
			this.set(fslCommandPath, "fslCommandPath");
		} catch (InvalidPreferenceName invalidPreferenceName) {
			log.error("Invalid preference Name", invalidPreferenceName);
		}
	}

	@NrgPreference
	public String getStructuralPreprocResource() {
		return this.getValue("structuralPreprocResource");
	}

	public void setStructuralPreprocResource(final String structuralPreprocResource) {
		try {
			this.set(structuralPreprocResource, "structuralPreprocResource");
		} catch (InvalidPreferenceName invalidPreferenceName) {
			log.error("Invalid preference Name", invalidPreferenceName);
		}
	}

	@NrgPreference
	public String getHandEditingResource() {
		return this.getValue("handEditingResource");
	}

	public void setHandEditingResource(final String handEditingResource) {
		try {
			this.set(handEditingResource, "handEditingResource");
		} catch (InvalidPreferenceName invalidPreferenceName) {
			log.error("Invalid preference Name", invalidPreferenceName);
		}
	}

	@NrgPreference
	public String getStructuralPreprocHandEditingResource() {
		return this.getValue("structuralPreprocHandEditingResource");
	}

	public void setStructuralPreprocHandEditingResource(final String structuralPreprocHandEditingResource) {
		try {
			this.set(structuralPreprocHandEditingResource, "structuralPreprocHandEditingResource");
		} catch (InvalidPreferenceName invalidPreferenceName) {
			log.error("Invalid preference Name", invalidPreferenceName);
		}
	}

	@NrgPreference
	public String getNotificationList() {
		return this.getValue("notificationList");
	}

	public void setNotificationList(final String notificationList) {
		try {
			this.set(notificationList, "notificationList");
		} catch (InvalidPreferenceName invalidPreferenceName) {
			log.error("Invalid preference Name", invalidPreferenceName);
		}
	}
	
	public StructuralQcSettings getStructuralQcSettings() {
		final StructuralQcSettings settings = new StructuralQcSettings();
		settings.setStructuralQcScript(this.getStructuralQcScript());
		settings.setTemplatesFolder(this.getTemplatesFolder());
		settings.setTemplateType(this.getTemplateType());
		settings.setWbCommandPath(this.getWbCommandPath());
		settings.setFslCommandPath(this.getFslCommandPath());
		settings.setStructuralPreprocResource(this.getStructuralPreprocResource());
		settings.setHandEditingResource(this.getHandEditingResource());
		settings.setStructuralPreprocHandEditingResource(this.getStructuralPreprocHandEditingResource());
		settings.setNotificationList(this.getNotificationList());
		return settings;
	}

}
