package org.nrg.structuralqc.components;

import org.nrg.ccf.common.utilities.abst.AbstractProjectPreferenceBean;
import org.nrg.ccf.common.utilities.components.PreferenceUtils;
import org.nrg.prefs.annotations.NrgPreference;
import org.nrg.prefs.annotations.NrgPreferenceBean;
import org.nrg.prefs.exceptions.InvalidPreferenceName;
import org.nrg.prefs.services.NrgPreferenceService;
import org.nrg.structuralqc.pojos.StructuralQcSettings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
@NrgPreferenceBean(toolId = "structuralQcProject", toolName = "Structural QC Project Settings")
public class ProjectSettings extends AbstractProjectPreferenceBean {

	private static final long serialVersionUID = 845526565106819081L;

	@Autowired
	protected ProjectSettings(NrgPreferenceService preferenceService, PreferenceUtils preferenceUtils) {
		super(preferenceService, preferenceUtils);
	}
	
	@NrgPreference
	public Boolean getStructuralQcEnabled() {
		return false;
	}

	public Boolean getStructuralQcEnabled(final String entityId) {
		return this.getBooleanValue(SCOPE, entityId, "structuralQcEnabled");
	}

	public void setStructuralQcEnabled(final String entityId, final Boolean structuralQcEnabled) {
		try {
			this.setBooleanValue(SCOPE, entityId, structuralQcEnabled, "structuralQcEnabled");
		} catch (InvalidPreferenceName invalidPreferenceName) {
			log.error("Invalid preference Name", invalidPreferenceName);
		}
	}

	@NrgPreference
	public String getStructuralQcScript() {
		return "";
	}
	
	public String getStructuralQcScript(final String entityId) {
		return this.getValue(SCOPE, entityId, "structuralQcScript");
	}

	public void setStructuralQcScript(final String entityId, final String structuralQcScript) {
		try {
			this.set(SCOPE, entityId, structuralQcScript, "structuralQcScript");
		} catch (InvalidPreferenceName invalidPreferenceName) {
			log.error("Invalid preference Name", invalidPreferenceName);
		}
	}

	@NrgPreference
	public String getTemplatesFolder() {
		return "";
	}

	public String getTemplatesFolder(final String entityId) {
		return this.getValue(SCOPE, entityId, "templatesFolder");
	}

	public void setTemplatesFolder(final String entityId, final String templatesFolder) {
		try {
			this.set(SCOPE, entityId, templatesFolder, "templatesFolder");
		} catch (InvalidPreferenceName invalidPreferenceName) {
			log.error("Invalid preference Name", invalidPreferenceName);
		}
	}

	@NrgPreference
	public String getTemplateType() {
		return "";
	}

	public String getTemplateType(final String entityId) {
		return this.getValue(SCOPE, entityId, "templateType");
	}

	public void setTemplateType(final String entityId, final String templateType) {
		try {
			this.set(SCOPE, entityId, templateType, "templateType");
		} catch (InvalidPreferenceName invalidPreferenceName) {
			log.error("Invalid preference Name", invalidPreferenceName);
		}
	}

	@NrgPreference
	public String getWbCommandPath() {
		return "";
	}

	public String getWbCommandPath(final String entityId) {
		return this.getValue(SCOPE, entityId, "wbCommandPath");
	}

	public void setWbCommandPath(final String entityId, final String wbCommandPath) {
		try {
			this.set(SCOPE, entityId, wbCommandPath, "wbCommandPath");
		} catch (InvalidPreferenceName invalidPreferenceName) {
			log.error("Invalid preference Name", invalidPreferenceName);
		}
	}

	@NrgPreference
	public String getFslCommandPath() {
		return "";
	}

	public String getFslCommandPath(final String entityId) {
		return this.getValue(SCOPE, entityId, "fslCommandPath");
	}

	public void setFslCommandPath(final String entityId, final String fslCommandPath) {
		try {
			this.set(SCOPE, entityId, fslCommandPath, "fslCommandPath");
		} catch (InvalidPreferenceName invalidPreferenceName) {
			log.error("Invalid preference Name", invalidPreferenceName);
		}
	}

	@NrgPreference
	public String getStructuralPreprocResource() {
		return "";
	}
	
	public String getStructuralPreprocResource(final String entityId) {
		return this.getValue(SCOPE, entityId, "structuralPreprocResource");
	}

	public void setStructuralPreprocResource(final String entityId, final String structuralPreprocResource) {
		try {
			this.set(SCOPE, entityId, structuralPreprocResource, "structuralPreprocResource");
		} catch (InvalidPreferenceName invalidPreferenceName) {
			log.error("Invalid preference Name", invalidPreferenceName);
		}
	}

	@NrgPreference
	public String getHandEditingResource() {
		return "";
	}
	
	public String getHandEditingResource(final String entityId) {
		return this.getValue(SCOPE, entityId, "handEditingResource");
	}

	public void setHandEditingResource(final String entityId, final String handEditingResource) {
		try {
			this.set(SCOPE, entityId, handEditingResource, "handEditingResource");
		} catch (InvalidPreferenceName invalidPreferenceName) {
			log.error("Invalid preference Name", invalidPreferenceName);
		}
	}

	@NrgPreference
	public String getStructuralPreprocHandEditingResource() {
		return "";
	}
	
	public String getStructuralPreprocHandEditingResource(final String entityId) {
		return this.getValue(SCOPE, entityId, "structuralPreprocHandEditingResource");
	}

	public void setStructuralPreprocHandEditingResource(final String entityId, final String structuralPreprocHandEditingResource) {
		try {
			this.set(SCOPE, entityId, structuralPreprocHandEditingResource, "structuralPreprocHandEditingResource");
		} catch (InvalidPreferenceName invalidPreferenceName) {
			log.error("Invalid preference Name", invalidPreferenceName);
		}
	}

	@NrgPreference
	public String getNotificationList() {
		return "";
	}
	
	public String getNotificationList(final String entityId) {
		return this.getValue(SCOPE, entityId, "notificationList");
	}

	public void setNotificationList(final String entityId, final String notificationList) {
		try {
			this.set(SCOPE, entityId, notificationList, "notificationList");
		} catch (InvalidPreferenceName invalidPreferenceName) {
			log.error("Invalid preference Name", invalidPreferenceName);
		}
	}
	
	public StructuralQcSettings getStructuralQcSettings(String projectId) {
		final StructuralQcSettings settings = new StructuralQcSettings();
		settings.setStructuralQcEnabled(this.getStructuralQcEnabled(projectId));
		settings.setStructuralQcScript(this.getStructuralQcScript(projectId));
		settings.setTemplatesFolder(this.getTemplatesFolder(projectId));
		settings.setTemplateType(this.getTemplateType(projectId));
		settings.setWbCommandPath(this.getWbCommandPath(projectId));
		settings.setFslCommandPath(this.getFslCommandPath(projectId));
		settings.setStructuralPreprocResource(this.getStructuralPreprocResource(projectId));
		settings.setHandEditingResource(this.getHandEditingResource(projectId));
		settings.setStructuralPreprocHandEditingResource(this.getStructuralPreprocHandEditingResource(projectId));
		settings.setNotificationList(this.getNotificationList(projectId));
		return settings;
	}

}
