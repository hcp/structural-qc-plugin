package org.nrg.structuralqc.utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOCase;
import org.apache.commons.io.filefilter.DirectoryFileFilter;
import org.apache.commons.io.filefilter.FalseFileFilter;
import org.apache.commons.io.filefilter.FileFilterUtils;
import org.apache.commons.io.filefilter.NameFileFilter;
import org.apache.commons.io.filefilter.RegexFileFilter;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.nrg.action.ClientException;
import org.nrg.action.ServerException;
import org.nrg.ccf.common.utilities.components.ScriptResult;
import org.nrg.ccf.common.utilities.constants.CommonConstants;
import org.nrg.ccf.common.utilities.utils.ResourceUtils;
import org.nrg.ccf.common.utilities.utils.ScriptExecUtils;
import org.nrg.ccf.hcppipelines.pcpcomponents.contstants.PipelineConstants;
import org.nrg.structuralqc.exception.StructuralQcPluginException;
import org.nrg.structuralqc.pojos.StructuralQcSettings;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatResourcecatalog;
import org.nrg.xft.security.UserI;
import org.nrg.xnat.services.archive.CatalogService;
import org.nrg.xnat.services.archive.CatalogService.Operation;
import org.nrg.xnat.turbine.utils.ArcSpecManager;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class StructuralQcUtils {
	
	final static String BACKUP_RESOURCE_LABEL = PipelineConstants.STRUC_PREPROC_RESOURCE_LABEL + "_orig";
	
	public static final String[] FILE_FILTER_ARRAY = new String[] { "brainmask.mgz", "wm.gz", "wm.mgz", "control.dat", 
			"aseg.presurf.mgz", "brain.finalsurfs.mgz", "T1.mgz", "lh.white", "lh.pial", "rh.white", "rh.pial",
			"rh.cortex.label", "lh.cortex.label", "rh.aparc.annot", "lh.aparc.annot", "rh.aparc.a2009s.annot", 
			"lh.aparc.a2009s.annot", 
			"rh.orig", "lh.orig", "rh.orig.nofix", "lh.orig.nofix", "rh.smoothwm.nofix", "lh.smoothwm.nofix", 
			"rh.woT2.pial", "lh.woT2.pial", 
			"T2.mgz", "norm.mgz", "aparc+aseg.mgz", "aparc.a2009s+aseg.mgz", "ribbon.mgz", 
			"rawavg.T2.norm.mgz", "rawavg.norm.mgz"
	};
	
    public static boolean hasStructuralQcOutput(XnatResourcecatalog preprocResource) {
    	for (final Object fileNameObj : preprocResource.getCorrespondingFileNames(CommonConstants.ROOT_PATH)) {
    		final String lcFileName = fileNameObj.toString().toLowerCase();
    		if (lcFileName.endsWith("scene") && lcFileName.contains("structuralqc")) {
    			return true;
    		}
    	}
    	return false;
	}

	public static ScriptResult runStructuralQcProcessing(XnatMrsessiondata mrSession, StructuralQcSettings settings) {
		
		final String studyFolder = getExperimentResourceFolder(mrSession) + "/" + settings.getStructuralPreprocResource();
		return runStructuralQcProcessing(mrSession, settings, studyFolder);
		
	}

	public static ScriptResult runStructuralQcHandEditingProcessing(XnatMrsessiondata mrSession, StructuralQcSettings settings) {
		
		final String studyFolder = getExperimentResourceFolder(mrSession) + "/" + settings.getStructuralPreprocHandEditingResource();
		return runStructuralQcProcessing(mrSession, settings, studyFolder);
		
	}
		
	public static ScriptResult runStructuralQcProcessing(XnatMrsessiondata mrSession, StructuralQcSettings settings, String studyFolder) {
		
		return ScriptExecUtils.execRuntimeCommand(new String[] { "/bin/bash","-c",settings.getStructuralQcScript() + " --subj-list " + 
				mrSession.getLabel() + " --templates-folder " + settings.getTemplatesFolder() + " --study-folder " + studyFolder +
				" --copy-templates " + " --copy-templates-as " + settings.getTemplateType() + " --wb-command-path " + settings.getWbCommandPath() +
				" --fsl-path " + settings.getFslCommandPath() + " --verbose" }, 999 );

	}

	public static String getExperimentResourceFolder(XnatMrsessiondata mrSession) {
		final String archivePath = ArcSpecManager.GetInstance().getArchivePathForProject(mrSession.getProject());
		final String sessionLabel = mrSession.getLabel();
		return archivePath + "arc001" + File.separator + sessionLabel + "/RESOURCES";
	}

	public static XnatResourcecatalog getHandEditingResource(XnatMrsessiondata mrSession, StructuralQcSettings settings, UserI user) throws StructuralQcPluginException {
		if (mrSession == null) {
			throw new StructuralQcPluginException("ERROR:  Requested experiment is null or is not an MR Session");
		}
		final XnatResourcecatalog handeditResource = ResourceUtils.getResource(mrSession, settings.getHandEditingResource());
		return handeditResource;
	}

	public static XnatResourcecatalog setupHandEditing(XnatMrsessiondata mrSession, StructuralQcSettings settings, UserI user, Boolean refreshOnly) throws StructuralQcPluginException {
		if (mrSession == null) {
			throw new StructuralQcPluginException("ERROR:  Requested experiment is null or is not an MR Session");
		}
		final XnatResourcecatalog preprocResource = ResourceUtils.getResource(mrSession, settings.getStructuralPreprocResource());
		XnatResourcecatalog handeditResource = ResourceUtils.getResource(mrSession, settings.getHandEditingResource());
		if (preprocResource == null) {
			throw new StructuralQcPluginException("ERROR:  Structural preprocessing has not been run for this session");
		}
		if (!StructuralQcUtils.hasStructuralQcOutput(preprocResource)) {
			throw new StructuralQcPluginException("ERROR:  StructuralQC has not been run for this session");
		}
		if (handeditResource == null) {
			try {
				handeditResource = ResourceUtils.createSessionResource(mrSession, settings.getHandEditingResource(), "MISC", "MISC", user);
			} catch (ServerException | ClientException e) {
				throw new StructuralQcPluginException("ERROR:  Exception thrown creating hand editing resource", e);
			}
		}
		try {
			return StructuralQcUtils.populateHandeditingResource(mrSession, handeditResource, preprocResource, refreshOnly);
		} catch (IOException e) {
			throw new StructuralQcPluginException("IOException thrown populating hand editing resource", e);
		}
	}

	private static XnatResourcecatalog populateHandeditingResource(XnatMrsessiondata mrSession, XnatResourcecatalog handeditResource,
			XnatResourcecatalog preprocResource, Boolean refreshOnly) throws StructuralQcPluginException, IOException {
		
		final String resourceFolder = getExperimentResourceFolder(mrSession);
		final String preprocFolder = resourceFolder + "/" + preprocResource.getLabel();
		final File srcQC = new File(preprocFolder + "/" + mrSession.getLabel() + "/MNINonLinear/StructuralQC");
		final File srcFS = new File(preprocFolder + "/" + mrSession.getLabel() +  "/T1w/" + mrSession.getLabel());
		if (!(srcQC.exists() && srcQC.isDirectory() && srcFS.exists() && srcFS.isDirectory())) {
			throw new StructuralQcPluginException("ERROR:  One or more required preprocessing directories does not exist.");
		}
		
		final String handeditFolder = resourceFolder + "/" + handeditResource.getLabel();
		//final File destQC = new File(handeditFolder + "/" + mrSession.getLabel() + "/MNINonLinear", "StructuralQC");
		//if (!(destQC.getParentFile().exists())) {
		//	destQC.getParentFile().mkdirs();
		//}
		//if (destQC.exists() && destQC.isDirectory()) {
		//	FileUtils.deleteDirectory(destQC);
		//}
		//FileUtils.copyDirectory(srcQC, destQC, true);
		final File destFS = new File(handeditFolder + "/" + mrSession.getLabel() + "/T1w", mrSession.getLabel());
		if (!(destFS.getParentFile().exists())) {
			destFS.getParentFile().mkdirs();
		}
		if (destFS.exists() && destFS.isDirectory() && !refreshOnly) {
			FileUtils.deleteDirectory(destFS);
		}
		final String[] filterArray;
		if (refreshOnly) {
			final List<String> fileList = new ArrayList<>(Arrays.asList(FILE_FILTER_ARRAY));
			for (final File f : FileUtils.listFiles(destFS, new NameFileFilter(FILE_FILTER_ARRAY), TrueFileFilter.INSTANCE)) {
				final Iterator<String> i = fileList.iterator();
				while (i.hasNext()) {
					if (f.getName().equals(i.next())) {
						i.remove();
					}
				}
			}
			final String[] newArr = new String[fileList.size()];
			filterArray = fileList.toArray(newArr);
		} else {
			filterArray = FILE_FILTER_ARRAY;
		}
		FileUtils.copyDirectory(srcFS, destFS, 
				FileFilterUtils.or(DirectoryFileFilter.DIRECTORY, 
						new NameFileFilter(
								filterArray
							)
				), true);
		// Prune empty directories
		List<File> fileL = new ArrayList<File>();
		fileL.addAll(FileUtils.listFilesAndDirs(destFS, FalseFileFilter.INSTANCE, TrueFileFilter.INSTANCE));
		// Order by longest paths
		Collections.sort(fileL, new Comparator<File>() {
			@Override
			public int compare(File o1, File o2) {
				return o2.getPath().length()-o1.getPath().length();
			}
		});
		for (File dirF : fileL) {
			if (dirF.isDirectory() && !hasFiles(dirF)) { 
				FileUtils.deleteDirectory(dirF);
			}
		}
		//final Iterator<File> fileI = FileUtils.iterateFiles(destFS, new NameFileFilter(
		//		new String[] { "brainmask.mgz", "wm.gz", "wm.mgz", "control.dat", "aseg.presurf.mgz" }
		//		), TrueFileFilter.INSTANCE);
		//while (fileI.hasNext()) {
		//	final File backupFile = fileI.next();
		//	final File destFile = new File(backupFile.getCanonicalPath() + ".orig");
		//	FileUtils.copyFile(backupFile, destFile);
		//}
		return handeditResource;
		
	}

	private static boolean hasFiles(File dirF) {
		if (dirF == null || !dirF.isDirectory()) {
			return false;
		}
		Collection<File> fileL = FileUtils.listFilesAndDirs(dirF, TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE);
		Iterator<File> i = fileL.iterator();
		while (i.hasNext()) {
			if (i.next().isFile()) {
				return true;
				
			}
		}
		return false;
	}

	public static boolean hasHandEditingSetup(XnatMrsessiondata mrSession, StructuralQcSettings settings) {
		final XnatResourcecatalog handeditResource = ResourceUtils.getResource(mrSession, settings.getHandEditingResource());
		if (handeditResource == null) {
			return false;
		}
		final String resourceFolder = getExperimentResourceFolder(mrSession);
		final String handeditFolder = resourceFolder + "/" + handeditResource.getLabel();
		//final File destQC = new File(handeditFolder "/" + mrSession.getLabel() + "/StructuralQC");
		final File destT1 = new File(handeditFolder + "/" + mrSession.getLabel() + "/T1w");
		//if (destQC.exists() && destQC.isDirectory() && destT1.exists() && destT1.isDirectory()) {
		if (destT1.exists() && destT1.isDirectory()) {
			return true;
		}
		return false;
	}

	public static boolean replaceHandEditingOutput(XnatMrsessiondata mrSession, CatalogService _catalogService, UserI user) throws StructuralQcPluginException {
		final XnatResourcecatalog preprocResource = ResourceUtils.getResource(mrSession, PipelineConstants.STRUC_PREPROC_RESOURCE_LABEL);
		final XnatResourcecatalog handeditResource = ResourceUtils.getResource(mrSession, PipelineConstants.STRUC_HANDEDIT_RESOURCE_LABEL);
		if (preprocResource == null || handeditResource == null) {
			throw new StructuralQcPluginException("ERROR:  Could not access one or both of the source resources.");
		}
		final String preprocResourcePath = ResourceUtils.getResourcePath(preprocResource);
		final String handeditResourcePath = ResourceUtils.getResourcePath(handeditResource);
		if (preprocResourcePath == null || handeditResourcePath == null) {
			throw new StructuralQcPluginException("ERROR:  Could not access one or both of the source resource paths.");
		}
		final File preprocResourcePathF = new File(preprocResourcePath);
		final File handeditResourcePathF = new File(handeditResourcePath);
		
		// Backup original structural preprocessing output if it hasn't already been
		if (!ResourceUtils.hasResource(mrSession, BACKUP_RESOURCE_LABEL)) {
			try {
				final XnatResourcecatalog backupResource = ResourceUtils.createSessionResource(mrSession, BACKUP_RESOURCE_LABEL, user);
				final String backupResourcePath = ResourceUtils.getResourcePath(backupResource);
				if (backupResourcePath == null) {
					throw new StructuralQcPluginException("ERROR:  Could not access the backup resource path.");
				}
				final File backupResourcePathF = new File(backupResourcePath);
				FileUtils.copyDirectory(preprocResourcePathF, backupResourcePathF, 
						new RegexFileFilter("^.*(?<!catalog.xml)$", IOCase.INSENSITIVE), true);
				final StringBuilder resourceSB = new StringBuilder("/archive/projects/").append(mrSession.getProject())
				.append("/subjects/").append(mrSession.getSubjectId()).append("/experiments/").append(mrSession.getId())
				.append("/resources/").append(BACKUP_RESOURCE_LABEL);
				_catalogService.refreshResourceCatalog(user, resourceSB.toString(), Operation.ALL);
			} catch (ServerException | ClientException | IOException e) {
				throw new StructuralQcPluginException("ERROR:  Could not create backup resource (" + BACKUP_RESOURCE_LABEL + ")", e);
			} 
		} else {
			log.debug(BACKUP_RESOURCE_LABEL + " resource already exists.  Not backing up current output");
		}
		
		boolean copyOkay = true;
		for (final File newF : FileUtils.listFilesAndDirs(handeditResourcePathF, TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE)) {
			if (newF.isDirectory() || newF.getName().matches("(?i)^.*catalog.xml")) {
				continue;
			}
			String newFP;
			try {
				newFP = newF.getCanonicalPath();
			} catch (IOException e) {
				newFP = newF.getAbsolutePath();
			}
			final String destFP = newFP.replaceAll(File.separator + PipelineConstants.STRUC_HANDEDIT_RESOURCE_LABEL + File.separator,
					File.separator + PipelineConstants.STRUC_PREPROC_RESOURCE_LABEL + File.separator);
			if (newFP.equals(destFP)) {
				copyOkay = false;
				continue;
			}
			try {
				FileUtils.copyFile(newF, new File(destFP), true);
			} catch (IOException e) {
				copyOkay = false;
			}
		}
		// Touch success file modification time to trigger PCP prereq check flag for next pipeline
		for (final File successF : FileUtils.listFilesAndDirs(preprocResourcePathF, new RegexFileFilter("^.*[.]success$") 
				, TrueFileFilter.INSTANCE)) {
			successF.setLastModified(System.currentTimeMillis());
		}
		final StringBuilder resourceSB = new StringBuilder("/archive/projects/").append(mrSession.getProject())
		.append("/subjects/").append(mrSession.getSubjectId()).append("/experiments/").append(mrSession.getId())
		.append("/resources/").append(PipelineConstants.STRUC_PREPROC_RESOURCE_LABEL);
		try {
			_catalogService.refreshResourceCatalog(user, resourceSB.toString(), Operation.ALL);
		} catch (ServerException | ClientException e) {
			log.warn("WARNING:  Could not refresh resource catalog (" + PipelineConstants.STRUC_HANDEDIT_RESOURCE_LABEL + ")");
		}
		return copyOkay;
	}

}
