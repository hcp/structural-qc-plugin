package org.nrg.structuralqc.exception;

public class StructuralQcPluginException extends Exception{
	
	private static final long serialVersionUID = -8317054868410646532L;

	public StructuralQcPluginException(String msg,Throwable e){
		super(msg,e);
	}
	
	public StructuralQcPluginException(String msg){
		super(msg);
	}
	
}

