package org.nrg.structuralqc.conf;

import lombok.extern.slf4j.Slf4j;
import org.nrg.framework.annotations.XnatPlugin;
import org.springframework.context.annotation.ComponentScan;

@XnatPlugin(
			value = "structuralQcPlugin",
			name = "Structural QC Plugin",
			log4jPropertiesFile = "/META-INF/resources/structuralQcLog4j.properties"
		)
@ComponentScan({ 
		"org.nrg.structuralqc.conf",
		"org.nrg.structuralqc.components",
		//"org.nrg.structuralqc.event.listeners",
		"org.nrg.structuralqc.utils",
		"org.nrg.structuralqc.xapi"
	})
@Slf4j
public class CcfStructuralQcPlugin {
	
	public CcfStructuralQcPlugin() {
		log.info("Configuring the Structural QC Plugin.");
	}
	
}
