package org.nrg.structuralqc.importers;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.nrg.action.ClientException;
import org.nrg.action.ServerException;
import org.nrg.ccf.linkeddata.AbstractLinkedDataImporter;
import org.nrg.structuralqc.components.ProjectSettings;
import org.nrg.structuralqc.exception.StructuralQcPluginException;
import org.nrg.structuralqc.pojos.StructuralQcSettings;
import org.nrg.structuralqc.utils.StructuralQcUtils;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatResourcecatalog;
import org.nrg.xft.security.UserI;
import org.nrg.xnat.restlet.util.FileWriterWrapperI;
import org.nrg.xnat.services.archive.CatalogService;
import org.nrg.xnat.services.archive.CatalogService.Operation;

public class HandEditingImporter extends AbstractLinkedDataImporter {
	
	private String FINALSURFS_FILENAME = "brain.finalsurfs.mgz";
	private String CONTROLDAT_FILENAME = "control.dat";
	private String FINALSURFS_MANEDIT_FILENAME = "brain.finalsurfs.manedit.mgz";
	private final String BACKUP_EXT = ".orig";

	public HandEditingImporter(UserI u, Map<String, Object> params) {
		super(u, params);
	}
	
	public HandEditingImporter(Object listenerControl, UserI u, FileWriterWrapperI fw, Map<String, Object> params) {
		super(listenerControl, u, fw, params);
	}

	@Override
	public void processCacheFiles(File buildDir) throws ClientException, ServerException {
		
		final XnatMrsessiondata mrSession = (XnatMrsessiondata)exp;
		final ProjectSettings projectSettings = XDAT.getContextService().getBean(ProjectSettings.class);
		final StructuralQcSettings settings = projectSettings.getStructuralQcSettings(proj.getId());
		final String resourceFolder = StructuralQcUtils.getExperimentResourceFolder(mrSession);
		XnatResourcecatalog handeditResource = null;
		try {
			handeditResource = StructuralQcUtils.getHandEditingResource(mrSession, settings, user);
		} catch (StructuralQcPluginException e) {
			throw new ServerException(e);
		}
		if (handeditResource == null) {
			throw new ClientException("ERROR:  Could not obtain hand editing resource.  Does it exist?");
		}
		final String handeditFolder = resourceFolder + "/" + handeditResource.getLabel();
		final File handeditDir = new File(handeditFolder);
		if (!(handeditDir.exists() && handeditDir.isDirectory() && handeditDir.canWrite())) {
			throw new ClientException("ERROR:  Hand editing resource folder doesn't exist, isn't a directory or isn't writable.");
		}
		final CatalogService catalogService = XDAT.getContextService().getBean(CatalogService.class);
		if (catalogService == null) {
			throw new ServerException("Exception thrown:  CatalogService is not available");
		}
		try {
			final Collection<File> fileC = FileUtils.listFiles(buildDir, TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE);
			final Iterator<File> i = fileC.iterator();
			while (i.hasNext()) {
				final File f = i.next();
				try {
					processUpload(f, handeditResource, handeditDir);
				} catch (IOException e) {
					throw new ServerException("ERROR:  Couldn't process file (FILE=" + f.getName() + ")", e);
				}
			}
		} finally {
			final StringBuilder resourceSB = new StringBuilder("/archive/projects/").append(exp.getProject())
					.append("/subjects/").append(exp.getSubjectId()).append("/experiments/").append(exp.getId())
					.append("/resources/").append(handeditResource.getLabel());
			catalogService.refreshResourceCatalog(user, resourceSB.toString(), Operation.ALL);
			this.getReturnList().add("Catalog refresh successful (RESOURCE=" + handeditResource.getLabel() + ")");
		}
		
	}

	private void processUpload(File currFile, XnatResourcecatalog handeditResource, File handeditDir) throws IOException, ClientException, ServerException {
		final Collection<File> fileC = FileUtils.listFiles(handeditDir, TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE);
		if (!(params.containsKey("OverrideSizeError") && 
				params.get("OverrideSizeError").toString().toLowerCase().startsWith("true"))) {
			doFilesizeCheck(fileC, currFile);
		}
		final String currFileName = currFile.getName();
		if (currFileName.matches("^.*" + BACKUP_EXT + "$") || currFileName.matches("^.*[.]edit_[0-9]+$")) {
			this.getReturnList().add("WARNING:  Unexpected file (backup or versioned file).  File <b>" + currFile.getName() + "</b> was not processed.<br>");
			return;
		}
		File baseDir = null;
		final Iterator<File> i = fileC.iterator();
		boolean fileHandled = false;
		while (i.hasNext()) {
			final File f = i.next();
			if (!(f.exists() && f.isFile())) {
				continue;
			}
			final File parentFile = f.getParentFile();
			if (baseDir == null && (parentFile.getName().equals("surf") || parentFile.getName().equals("mri"))) {
				baseDir = parentFile.getParentFile();
				
			}
			if (f.getName().equals(currFile.getName()) && !currFile.getName().equals(FINALSURFS_FILENAME) && !currFile.getName().equals(CONTROLDAT_FILENAME)) {
				
				if (fileUnchanged(f, currFile)) {	
					fileHandled = true;
					break;
				}
				
				final File destFile = new File(f.getCanonicalPath() + BACKUP_EXT);
				if (!destFile.exists()) {
					FileUtils.moveFile(f, destFile);
					this.getReturnList().add("Moved <b>" + editDisplayPath(f.getPath()) + "</b> to <b>" + 
							editDisplayPath(destFile.getPath()) + "</b><br>");
				} else {
					this.getReturnList().add("Backup of original already exists <b>(" + editDisplayPath(destFile.getPath()) + ")</b>.  Create edit file.");
					if (f.exists()) {
						final String editExt = getEditExtension(f);
						if (editExt != null) {
							final File editFile = new File(f.getCanonicalPath() + editExt);
							FileUtils.moveFile(f, editFile);
							this.getReturnList().add("Create edit version file for previously uploaded file - <b>" + editFile.getName() + "</b>");
						} else {
							// File same as previous edit file or too many versions.  Don't keep the current one.
							FileUtils.deleteQuietly(f);
						}
					}
				}
				FileUtils.moveFile(currFile, f);
				this.getReturnList().add("Uploaded <b>" + currFile.getName() + "</b> to <b>" + 
						editDisplayPath(f.getPath()) + "</b><br>");
				fileHandled = true;
				break;
				
			} else if ((f.getName().equals(currFile.getName()) && currFile.getName().equals(FINALSURFS_FILENAME)) ||
						(f.getName().equals(FINALSURFS_FILENAME) && currFile.getName().equals(FINALSURFS_MANEDIT_FILENAME))
					) {
				
				if (fileUnchanged(f, currFile)) {	
					fileHandled = true;
					break;
				}
				
				// We don't really need a backup of branmask.finalsurfs.mgz
				//final File destFile = new File(f.getCanonicalPath() + BACKUP_EXT);
				//FileUtils.copyFile(f, destFile);
				//this.getReturnList().add("Copied <b>" + editDisplayPath(f.getPath()) + "</b> to <b>" + 
				//			editDisplayPath(destFile.getPath()) + "</b><br>");
				final File finalsurfs = new File(f.getParent(), FINALSURFS_MANEDIT_FILENAME);
				if (finalsurfs.exists()) {
					final String editExt = getEditExtension(finalsurfs);
					if (editExt != null) {
						final File editFile = new File(f.getCanonicalPath() + editExt);
						FileUtils.moveFile(f, editFile);
						this.getReturnList().add("Create edit version file for previously uploaded file - <b>" + editFile.getName() + "</b>");
					} else {
						// File same as previous edit file or too many versions.  Don't keep the current one.
						FileUtils.deleteQuietly(finalsurfs);
					}
				}
				FileUtils.moveFile(currFile, finalsurfs);
				this.getReturnList().add("Uploaded <b>" + currFile.getName() + "</b> to <b>" + 
						editDisplayPath(finalsurfs.getPath()) + "</b><br>");
				fileHandled = true;
				break;
				
			} else if (f.getName().equals(currFile.getName()) && currFile.getName().equals(CONTROLDAT_FILENAME)) {
				
				if (fileUnchanged(f, currFile)) {	
					fileHandled = true;
					break;
				}
				
				// We don't really need a backup of control.dat
				//final File destFile = new File(f.getCanonicalPath() + BACKUP_EXT);
				//FileUtils.copyFile(f, destFile);
				//this.getReturnList().add("Copied <b>" + editDisplayPath(f.getPath()) + "</b> to <b>" + 
				//			editDisplayPath(destFile.getPath()) + "</b><br>");
				final File controldat = new File(f.getParent(), CONTROLDAT_FILENAME);
				if (controldat.exists()) {
					final String editExt = getEditExtension(controldat);
					if (editExt != null) {
						final File editFile = new File(f.getCanonicalPath() + editExt);
						FileUtils.moveFile(f, editFile);
						this.getReturnList().add("Create edit version file for previously uploaded file - <b>" + editFile.getName() + "</b>");
					} else {
						// File same as previous edit file or too many versions.  Don't keep the current one.
						FileUtils.deleteQuietly(controldat);
					}
				}
				FileUtils.moveFile(currFile, controldat);
				this.getReturnList().add("Uploaded <b>" + currFile.getName() + "</b> to <b>" + 
						editDisplayPath(controldat.getPath()) + "</b><br>");
				fileHandled = true;
				break;
			} 
		}
		if (fileHandled) {
			return;
		}
		if (baseDir != null && currFile.getName().equals(CONTROLDAT_FILENAME)) {
			
			// We don't really need a backup of control.dat
			//final File destFile = new File(f.getCanonicalPath() + BACKUP_EXT);
			//FileUtils.copyFile(f, destFile);
			//this.getReturnList().add("Copied <b>" + editDisplayPath(f.getPath()) + "</b> to <b>" + 
			//			editDisplayPath(destFile.getPath()) + "</b><br>");
			final File tmpDir = new File(baseDir, "tmp");
			if (!tmpDir.exists()) {
				tmpDir.mkdir();
			}
			final File controldat = new File(tmpDir, CONTROLDAT_FILENAME);
			FileUtils.moveFile(currFile, controldat);
			this.getReturnList().add("Uploaded <b>" + currFile.getName() + "</b> to <b>" + 
					editDisplayPath(controldat.getPath()) + "</b><br>");
		} else {
			this.getReturnList().add("WARNING:  Unexpected file.  File <b>" + currFile.getName() + "</b> was not uploaded.<br>");
		}
	}
	
	private String getEditExtension(File f) {
		for (int i=1; i<50; i++) {
			final String editExt = ".edit_" + i;
			File editFile = new File(f.getPath() + ".edit_" + i);
			if (!editFile.exists()) {
				return editExt;
			} 
			if (editFile.exists() && fileUnchanged(f, editFile)) {
				return null;
			}
		}
		return null;
	}

	private boolean fileUnchanged(File f, File currFile) {
		try {
			if (Arrays.equals(DigestUtils.md5(FileUtils.readFileToByteArray(f)), DigestUtils.md5(FileUtils.readFileToByteArray(currFile)))) {
					this.getReturnList().add("File <b>" + currFile.getName() + "</b> is unchanged from existing file.  Nothing done.<br>");
					return true;
			}
		} catch (IOException e) {
			// Just proceed, if possible.  It's okay to replace unchanged file.
		}
		return false;
	}

	private String editDisplayPath(String path) {
		if (path == null || path.length()<1) {
			return "";
		}
		return path.replaceFirst("^.*/RESOURCES/", "");
	}

	private void doFilesizeCheck(Collection<File> fileC, File currFile) throws ClientException {
		final Iterator<File> i = fileC.iterator();
		while (i.hasNext()) {
			final File f = i.next();
			if (!(f.exists() && f.isFile())) {
				continue;
			}
			if (f.getName().equals(currFile.getName()) && !currFile.getName().equals(FINALSURFS_FILENAME)) {
				checkSize(currFile, f);
			} else if (f.getName().equals(currFile.getName()) && currFile.getName().equals(FINALSURFS_FILENAME)) {
				checkSize(currFile, f);
			} else if (f.getName().equals(FINALSURFS_FILENAME) && currFile.getName().equals(FINALSURFS_MANEDIT_FILENAME)) {
				checkSize(currFile, f);
			}
		}
	}

	private void checkSize(File currFile, File f) throws ClientException {
		if (!(currFile.exists() && f.exists() && currFile.isFile() && f.isFile())) {
			return;
		}
		float percentAllowed = 2;
		if (params.get("MaxAllowedDifference") != null) {
			percentAllowed = Float.valueOf(params.get("MaxAllowedDifference").toString());
		}
		float pctDiff = (Math.abs((float)currFile.length()-(float)f.length())/(float)f.length());
		float checkDiff = (percentAllowed/100);
 		if (pctDiff>checkDiff) {
 			throw new ClientException("ERROR:  File " + f.getName() + 
 					" fails \"file size difference\" check.  Please check file is being uploaded to the correct subject! (" +
 					currFile.length() + " vs. " + f.length() + ")");
 		}
	}

}
