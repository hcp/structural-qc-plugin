package org.nrg.structuralqc.pojos;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.nrg.ccf.common.utilities.constants.CommonConstants;
import org.nrg.ccf.common.utilities.pojos.ResourceZipResponseBody;
import org.nrg.ccf.common.utilities.utils.ResourceUtils;
import org.nrg.xdat.om.XnatResourcecatalog;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

//import lombok.extern.slf4j.Slf4j;

//@Slf4j
public class StructuralQcZipResponseBody extends ResourceZipResponseBody implements StreamingResponseBody {
	
	public StructuralQcZipResponseBody(XnatResourcecatalog preprocResource) {
		super(preprocResource);
	}

	@Override
	protected void initializeZipList(XnatResourcecatalog preprocResource) throws IOException {
		final List<File> preprocFiles = ResourceUtils.getFiles(preprocResource, CommonConstants.ROOT_PATH);
		final List<String> filenameList = new ArrayList<>();
		File sceneFile = null;
		for (final File f : preprocFiles) {
			final String lcfn = f.getName().toLowerCase();
    		if (lcfn.endsWith("scene") && lcfn.contains("structuralqc")) {
    			sceneFile = f;
    			break;
    		}
		}
		if (sceneFile == null) {
			return;
		}
		String sceneFileContents = null;
		sceneFileContents = FileUtils.readFileToString(sceneFile);
		final BufferedReader breader = new BufferedReader(new StringReader(sceneFileContents));
		String line;
		while ((line = breader.readLine()) != null) {
			if (!line.contains("../")) {
				continue;
			}
			final String fileLine = line.replaceFirst(".*[.][.]/", "").replaceFirst("[]'\"].*$", "");
			if (!filenameList.contains(fileLine)) {
				//log.debug("fileLine=" + fileLine);
				filenameList.add(fileLine);
			}
		}
		breader.close();
		final String scenefileDir = sceneFile.getParentFile().getAbsolutePath();
		outerloop:
		for (final File f : preprocFiles) {
			final String filepath = f.getAbsolutePath();
			if (filepath.startsWith(scenefileDir)) {
				_zipList.add(f);
				continue;
			}
			for (final String fn : filenameList) {
				//log.debug("fn=" + fn + " , filepath=" + filepath);
				if (filepath.endsWith(fn)) {
					_zipList.add(f);
					continue outerloop;
					
				}
			}
		}
	}

}
