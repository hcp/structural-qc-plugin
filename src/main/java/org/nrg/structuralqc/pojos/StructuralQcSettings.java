package org.nrg.structuralqc.pojos;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StructuralQcSettings {
	
	private Boolean structuralQcEnabled; 
	private String structuralQcScript; 
	private String templatesFolder; 
	private String templateType; 
	private String wbCommandPath; 
	private String fslCommandPath; 
	private String structuralPreprocResource; 
	private String handEditingResource; 
	private String structuralPreprocHandEditingResource; 
	private String notificationList; 
	
}
