package org.nrg.structuralqc.pojos;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StructuralQcStatus {
	
	public Boolean structuralQcEnabled = false;
	public Boolean hasPreprocOutput = false;;
	public Boolean hasStructuralQcOutput = false;
	public Boolean hasHandEditingSetup = false;
	public Boolean hasRerunResults = false;
	public Boolean alreadyReplaced = false;
	
}
