

if (typeof CCF === 'undefined') {
	CCF = {};
}

if (typeof CCF.structuralQC === 'undefined') {
	CCF.structuralQC = {
		RUN_PROCESSING_URL: window.location.protocol + "//" + window.location.host + "/xapi/structuralQc/project/" + XNAT.data.context.projectID + "/subject/" +
					 XNAT.data.context.subjectID + "/experiment/" + XNAT.data.context.ID + "/runStructuralQcProcessing",
		RUN_HANDEDIT_PROCESSING_URL: window.location.protocol + "//" + window.location.host + "/xapi/structuralQc/project/" + XNAT.data.context.projectID + "/subject/" +
					 XNAT.data.context.subjectID + "/experiment/" + XNAT.data.context.ID + "/runStructuralQcHandEditingProcessing",
		STATUS_URL: window.location.protocol + "//" + window.location.host + "/xapi/structuralQc/project/" + XNAT.data.context.projectID + "/subject/" +
					 XNAT.data.context.subjectID + "/experiment/" + XNAT.data.context.ID + "/status",
		QC_DOWNLOAD_URL: window.location.protocol + "//" + window.location.host + "/xapi/structuralQc/project/" + XNAT.data.context.projectID + "/subject/" +
					 XNAT.data.context.subjectID + "/experiment/" + XNAT.data.context.ID + "/download",
		HANDEDIT_DOWNLOAD_URL: window.location.protocol + "//" + window.location.host + "/xapi/handEditingFiles/project/" + XNAT.data.context.projectID + "/subject/" +
					 XNAT.data.context.subjectID + "/experiment/" + XNAT.data.context.ID + "/download",
		HAND_EDITING_URL: window.location.protocol + "//" + window.location.host + "/xapi/structuralQc/project/" + XNAT.data.context.projectID + "/subject/" +
					 XNAT.data.context.subjectID + "/experiment/" + XNAT.data.context.ID + "/setupHandEditing",
		LAUNCH_PREPROC_URL: window.location.protocol + "//" + window.location.host + "/xapi/structuralQc/project/" + XNAT.data.context.projectID + "/subject/" +
					 XNAT.data.context.subjectID + "/experiment/" + XNAT.data.context.ID + "/launchHandEditingPreprocPipeline",
		REPLACE_PREPROC_URL: window.location.protocol + "//" + window.location.host + "/xapi/structuralQc/project/" + XNAT.data.context.projectID + "/subject/" +
					 XNAT.data.context.subjectID + "/experiment/" + XNAT.data.context.ID + "/replaceHandEditingOutput",
		PROCESSING_STATUS_URL: window.location.protocol + "//" + window.location.host + "/xapi/structuralQc/project/" + XNAT.data.context.projectID + "/subject/" +
					 XNAT.data.context.subjectID + "/experiment/" + XNAT.data.context.ID + "/checkProcessingStatus",
		STATUS_MODAL: {
		        id: 'statusModal',
		        kind: 'fixed',  
		        width: 650, 
		        height: 325, 
		        scroll: 'yes', 
		        title: 'StructuralQC Status', 
			content: "<div id='strucqc-content' class='strucqc-processing'><div id='modal-msg' style='margin-right:15px'></div></div>",
		        ok: 'show',  
		        okLabel: "Don't Wait",
		        okAction: function(){ xmodal.close() },
		        cancel: 'hide' 
		}

	}
}


///////////////////////
///////////////////////
///////////////////////
// SHOW LINK METHODS //
///////////////////////
///////////////////////
///////////////////////


CCF.structuralQC.checkShowLink = function() {
	CCF.structuralQC.getStructuralQcStatus();
}


////////////////////
////////////////////
////////////////////
// XMODAL METHODS //
////////////////////
////////////////////
////////////////////


CCF.structuralQC.modalProcessing = function(msg, title) {
    var modalObj = CCF.structuralQC.STATUS_MODAL;
    //console.log("title=",title);
    if (typeof title != "undefined") {
        modalObj = JSON.parse(JSON.stringify(CCF.structuralQC.STATUS_MODAL));
        modalObj.title = title;
    } 
    xModalOpenNew(modalObj);
    $("#strucqc-content").removeClass("strucqc-complete");
    $("#strucqc-content").removeClass("strucqc-error");
    $("#strucqc-content").addClass("strucqc-processing");
    $("#modal-msg").html(msg);
    $("#statusModal-ok-button").html("Don't Wait");
}
CCF.structuralQC.modalComplete = function(msg) {
    $("#strucqc-content").removeClass("strucqc-processing");
    $("#strucqc-content").removeClass("strucqc-error");
    $("#strucqc-content").addClass("strucqc-complete");
    $("#modal-msg").html(msg);
    $("#statusModal-ok-button").html("OK");
}
CCF.structuralQC.modalMessage = function(msg) {
    $("#strucqc-content").removeClass("strucqc-processing");
    $("#strucqc-content").removeClass("strucqc-error");
    $("#strucqc-content").removeClass("strucqc-complete");
    $("#modal-msg").html(msg);
    $("#statusModal-ok-button").html("OK");
}
CCF.structuralQC.modalError = function(msg) {
    $("#strucqc-content").removeClass("strucqc-processing");
    $("#strucqc-content").removeClass("strucqc-complete");
    $("#strucqc-content").addClass("strucqc-error");
    $("#modal-msg").html(msg);
    $("#statusModal-ok-button").html("OK");
}


////////////////////////////
////////////////////////////
////////////////////////////
// RERUN PIPELINE METHODS //
////////////////////////////
////////////////////////////
////////////////////////////


CCF.structuralQC.runStructuralQC = function() {

    CCF.structuralQC.modalProcessing("Processing");
    XNAT.xhr.post({
	type: "POST",
	url:CCF.structuralQC.RUN_PROCESSING_URL,
	cache: false,
	async: true,
	contentType:"text/plain; charset=utf-8",
	context: this
	//data: JSON.stringify(fixArr),
	//dataType: 'json'
    }).done( function(data, textStatus, jqXHR) {
  	$(".top-banner").hide();
    	CCF.structuralQC.modalComplete("Processing Complete");
	CCF.structuralQC.getStructuralQcStatus();
    }).fail( function(data, textStatus, jqXHR) {
    	CCF.structuralQC.modalError("ERROR:  Processing Failed (STATUS=" + textStatus + ")");
    });

}

CCF.structuralQC.runStructuralQcHandEdit = function() {

    CCF.structuralQC.modalProcessing("Processing");
    XNAT.xhr.post({
	type: "POST",
	url:CCF.structuralQC.RUN_HANDEDIT_PROCESSING_URL,
	cache: false,
	async: true,
	contentType:"text/plain; charset=utf-8",
	context: this
	//data: JSON.stringify(fixArr),
	//dataType: 'json'
    }).done( function(data, textStatus, jqXHR) {
  	$(".top-banner").hide();
    	CCF.structuralQC.modalComplete("Processing Complete");
	CCF.structuralQC.getStructuralQcStatus();
    }).fail( function(data, textStatus, jqXHR) {
    	CCF.structuralQC.modalError("ERROR:  Processing Failed (STATUS=" + textStatus + ")");
    });

}

//////////////////////////
//////////////////////////
//////////////////////////
// HAND EDITING METHODS //
//////////////////////////
//////////////////////////
//////////////////////////


CCF.structuralQC.setupHandEditing = function() {

	var structuralQcStatus = CCF.structuralQC.structuralQcStatus;

	if (typeof structuralQcStatus == 'undefined' || !structuralQcStatus.hasHandEditingSetup) {

		CCF.structuralQC.doHandEditingSetup();

	} else {

		xmodal.confirm({
			title:  "Overwrite existing files?",
			content:  "WARNING:  This will overwrite any existing edited files.  Are you sure you want to continue?",
			width: 500,
			height: 250,
			okLabel:  "OK",
			okClose:  true,
			okAction:  function() {
				CCF.structuralQC.doHandEditingSetup();
			}

	});

	}

}

CCF.structuralQC.doHandEditingSetup = function() {

    CCF.structuralQC.modalProcessing("Processing", "Hand Editing Status");
    XNAT.xhr.post({
	type: "POST",
	url:CCF.structuralQC.HAND_EDITING_URL + "?refreshOnly=false",
	cache: false,
	async: true,
	contentType:"text/plain; charset=utf-8",
	context: this
	//data: JSON.stringify(fixArr),
	//dataType: 'json'
    }).done( function(data, textStatus, jqXHR) {
  	$(".top-banner").hide();
    	CCF.structuralQC.modalComplete("The hand-editing resource has been created.");
	CCF.structuralQC.getStructuralQcStatus();
    }).fail( function(data, textStatus, jqXHR) {
    	CCF.structuralQC.modalError("ERROR:  Processing Failed (STATUS=" + textStatus + ")");
    });

}


CCF.structuralQC.refreshHandEditing = function() {

	var structuralQcStatus = CCF.structuralQC.structuralQcStatus;

	if (typeof structuralQcStatus == 'undefined' || !structuralQcStatus.hasHandEditingSetup) {

		CCF.structuralQC.refreshHandEditingSetup();

	} else {

		xmodal.confirm({
			title:  "Refresh current hand editing resource (get new files)?",
			content:  "NOTE:  This action only retieves new files that have been requested for inclusion in the hand editing resource." 
					+ "  It does not overwrite any existing files.",
			width: 500,
			height: 250,
			okLabel:  "OK",
			okClose:  true,
			okAction:  function() {
				CCF.structuralQC.refreshHandEditingSetup();
			}

	});

	}

}

CCF.structuralQC.refreshHandEditingSetup = function() {

    CCF.structuralQC.modalProcessing("Processing", "Hand Editing Status");
    XNAT.xhr.post({
	type: "POST",
	url:CCF.structuralQC.HAND_EDITING_URL + "?refreshOnly=true",
	cache: false,
	async: true,
	contentType:"text/plain; charset=utf-8",
	context: this
	//data: JSON.stringify(fixArr),
	//dataType: 'json'
    }).done( function(data, textStatus, jqXHR) {
  	$(".top-banner").hide();
    	CCF.structuralQC.modalComplete("The hand-editing resource has been refreshed.");
	CCF.structuralQC.getStructuralQcStatus();
    }).fail( function(data, textStatus, jqXHR) {
    	CCF.structuralQC.modalError("ERROR:  Processing Failed (STATUS=" + textStatus + ")");
    });

}

CCF.structuralQC.uploadHandEditingFiles = function() {

	var currentConfigs = XNAT.app.abu.uploaderConfig;
	var triggerId = undefined;
	XNAT.app.abu.automationEvents.forEach((element, index) => {
		if (element.scriptId == 'StructQcHandEditFileUpload' && element.entityId == XNAT.data.context.projectID) {
			triggerId = element.triggerId;
			return;
		}
	});
	if (typeof triggerId !== 'undefined') {
		// Use the automation uploader here
		$(".abuLink.uploadLink").show();
		XNAT.app.abu.uploaderConfig = [
			{
				"eventTriggerId":triggerId,
				"event":"UploadStructQcHandEditing",
				"eventScope":"prj",
				"launchFromCacheUploads":true,
				"launchFromResourceUploads":false,
				"launchWithoutUploads":false,
				"doNotUseUploader":false,
				"escapeHtml":false,
				"showExtractOption":false,
				"extractOptionChecked":false,
				"showCloseWindowOption":true,
				"closeWindowOptionChecked":false,
				"showEmailOption":true,
				"emailOptionChecked":false,
				"xAcontexts":["xnat:mrSessionData"],
				"parameters":[
					{"name":"OverrideSizeError","defaultVal":"false","description":"Override filesize difference check error?","type":"String","required":true},
					{"name":"MaxAllowedDifference","defaultVal":"2.0","description":"Maximum Percentage Filesize Difference","type":"Float","required":true}
				]
			}
		];
		XNAT.app.abu.showScanLinks();
		XNAT.app.abu.initializeAbuUploader('Upload');
		$(".abuLink.uploadLink").click();
		// TODO:  Can't run these here to restore any original configuration, but we may want to allow for that at some point
		//XNAT.app.abu.uploaderConfig = currentConfigs;
		//XNAT.app.abu.getAutomationHandlers();
	} else {
		CCF.structuralQC.modalError("ERROR:  StructuralQC is not set up correctly.  Please contact administrator.");
	}

}


//////////////////////
//////////////////////
//////////////////////
// STATUS METHODS //
//////////////////////
//////////////////////
//////////////////////


CCF.structuralQC.getStructuralQcStatus = function() {

    XNAT.xhr.get({
	url:CCF.structuralQC.STATUS_URL,
	cache: false,
	async: true,
	//contentType:"application/zip",
	context: this
	//data: JSON.stringify(fixArr),
    }).done( function(data, textStatus, jqXHR) {
	var structuralQcStatus = data;
	CCF.structuralQC.structuralQcStatus = structuralQcStatus;
	$(".structuralQcLink").show();
	$(".structuralQcLink").find("a").removeClass("strucqc-disabled");
	var runHtml = $(".runStructuralQc").find("a").html();
	if (typeof runHtml !== "undefined" && runHtml.length>0) {
		$(".runStructuralQc").find("a").html(runHtml.replace("Rerun Structural QC","Run Structural QC"));
	}
	$(".downloadHandEdit").find("a").hide();
	$(".refreshHandEdit").find("a").hide();
	$(".uploadHandEdit").find("a").hide();
	$(".checkProcessingStatus").find("a").hide();
	$(".launchPreprocPipeline").find("a").hide();
	$(".replacePreprocResults").find("a").hide();
	$(".runStructuralQcHandEdit").find("a").hide();
	if (!structuralQcStatus.structuralQcEnabled) {
		console.log("StructuralQC Not Enabled");
		$(".structuralQcMenu").hide();
	} else if (!structuralQcStatus.hasPreprocOutput) {
		$(".structuralQcLink").find("a").addClass("strucqc-disabled");
	} else if (!structuralQcStatus.hasStructuralQcOutput) {
		$(".structuralQcDownload").find("a").addClass("strucqc-disabled");
		$(".handEdit").find("a").addClass("strucqc-disabled");
	} else if (structuralQcStatus.hasStructuralQcOutput) {
		var runHtml = $(".runStructuralQc").find("a").html();
		//console.log("structuralQcStatus",structuralQcStatus);
		if (typeof runHtml !== "undefined" && runHtml.length>0) {
			$(".runStructuralQc").find("a").html(runHtml.replace("Run Structural QC","Rerun Structural QC"));
		}
		if (structuralQcStatus.hasHandEditingSetup) {
			var heHtml = $(".setupHandEdit").find("a").html();
			$(".setupHandEdit").find("a").html(heHtml.replace("Initiate Hand Editing","Regenerate Hand Editing Resource"));
			$(".refreshHandEdit").find("a").show();
			$(".downloadHandEdit").find("a").show();
			$(".uploadHandEdit").find("a").show();
			$(".launchPreprocPipeline").find("a").show();
			$(".checkProcessingStatus").find("a").show();
		} 
		if (structuralQcStatus.hasRerunResults && !structuralQcStatus.alreadyReplaced) {
			var heHtml = $(".setupHandEdit").find("a").html();
			$(".replacePreprocResults").find("a").show();
			$(".runStructuralQcHandEdit").find("a").show();
		} 
	}
    }).fail( function(data, textStatus, jqXHR) {
	// Do nothing
    });

}


//////////////////////
//////////////////////
//////////////////////
// DOWNLOAD METHODS //
//////////////////////
//////////////////////
//////////////////////


CCF.structuralQC.downloadStructuralQC = function() {

    CCF.structuralQC.modalProcessing("Building download package.&nbsp; Please wait.");
    XNAT.xhr.get({
	url:CCF.structuralQC.QC_DOWNLOAD_URL,
	cache: false,
	async: true,
	//contentType:"application/zip",
	context: this
	//data: JSON.stringify(fixArr),
    }).done( function(data, textStatus, jqXHR) {
	xmodal.close();
	window.location.href=CCF.structuralQC.QC_DOWNLOAD_URL;
    }).fail( function(data, textStatus, jqXHR) {
	CCF.structuralQC.modalError("ERROR:  Error creating zip archive (STATUS=" + textStatus + ").");
    });

}


CCF.structuralQC.downloadHandEditingFiles = function() {

    CCF.structuralQC.modalProcessing("Building download package.&nbsp; Please wait.");
    XNAT.xhr.get({
	url:CCF.structuralQC.HANDEDIT_DOWNLOAD_URL,
	cache: false,
	async: true,
	//contentType:"application/zip",
	context: this
	//data: JSON.stringify(fixArr),
    }).done( function(data, textStatus, jqXHR) {
	xmodal.close();
	window.location.href=CCF.structuralQC.HANDEDIT_DOWNLOAD_URL;
    }).fail( function(data, textStatus, jqXHR) {
	CCF.structuralQC.modalError("ERROR:  Error creating zip archive (STATUS=" + textStatus + ").");
    });

}

CCF.structuralQC.launchPreprocPipeline = function() {

	var reconAllSelect = {
		viewPanel: {
			kind: 'panel',
			header: false,
			footer: false,
			contents: {
				reconStages: {
					kind: 'panel.select.single',
					name: 'input-reconStages',
					label: 'Select Recon Stages:',
					//element: { style: { 'padding-left':'1px', 'padding-right':'1px', 'height':'150px', 'width':'250px', 'max-width':'300px' } },
					element: { style: { 'padding-left':'1px', 'padding-right':'1px', 'width':'250px' } },
					description: "(REQUIRED) Select recon stages to run based on edits.",
					options: {
						'-T2pial|-autorecon2-cp|-autorecon3':'CP [+WM] [+Brainmask]',
						'-T2pial|-autorecon2-wm|-autorecon3':'WM [+Brainmask]',
						'-T2pial|-autorecon-pial':'Brainmask Only',
						//'-autorecon2-cp':'-autorecon2-cp',
						//'-autorecon2-wm':'-autorecon2-wm',
						//'-autorecon3':'-autorecon3',
						//'-autorecon-pial':'-autorecon-pial',
						'-all':'recon-all -all'
					},
				}
			}
		}
	};
	xmodal.confirm({
			title:  "Launch pipeline?",
			content:  "<span class='reconStageSpan'><h3>Launch the structural preprocessing hand editing pipeline?</h3><br><br> </span>",
			width: 700,
			height: 350,
			okLabel:  "OK",
			okClose:  false,
			okAction:  function() {
				var reconAllStages = $("#input-recon-stages").val();
				CCF.structuralQC.reconAllStages = reconAllStages;
				if (typeof reconAllStages !== 'undefined' && reconAllStages !== null) {
					xmodal.close();
					CCF.structuralQC.doLaunchPreprocPipeline();
				} else {
					alert("You must select at least one recon-all stage to run");
				}
			}

	});
	XNAT.spawner.spawn(reconAllSelect).render($(".reconStageSpan")[0])
}

CCF.structuralQC.doLaunchPreprocPipeline = function() {

    CCF.structuralQC.modalProcessing("Launching", "Launching structural preprocessing hand editing pipeline");
    XNAT.xhr.post({
	url:CCF.structuralQC.LAUNCH_PREPROC_URL,
	cache: false,
	async: true,
	data: JSON.stringify(CCF.structuralQC.reconAllStages),
	context: this,
	contentType: 'application/json'
    }).done( function(data, textStatus, jqXHR) {
    	CCF.structuralQC.modalComplete("Successfully launched pipeline");
    }).fail( function(data, textStatus, jqXHR) {
	CCF.structuralQC.modalError(jqXHR.responseText);
    });

}


CCF.structuralQC.replacePreprocResults = function() {

	xmodal.confirm({
			title:  "Replace existing output?",
			content:  "Replace previous structural preprocessing output with hand edited processing results?",
			width: 500,
			height: 250,
			okLabel:  "OK",
			okClose:  true,
			okAction:  function() {
				CCF.structuralQC.doReplacePreprocResults();
			}

	});
}

CCF.structuralQC.doReplacePreprocResults = function() {

    CCF.structuralQC.modalProcessing("Replacing", "Replacing structural preprocessing pipeline output with hand editing pipeline output");
    XNAT.xhr.post({
	url:CCF.structuralQC.REPLACE_PREPROC_URL,
	cache: false,
	async: true,
	context: this
    }).done( function(data, textStatus, jqXHR) {
    	CCF.structuralQC.modalComplete("Output successfully replaced");
    }).fail( function(data, textStatus, jqXHR) {
	CCF.structuralQC.modalError("ERROR:  Could not replace output (STATUS=" + textStatus + ").");
    });

}


CCF.structuralQC.checkProcessingStatus = function() {

    CCF.structuralQC.modalProcessing("Processing Status", "Checking processing status.....");
    XNAT.xhr.get({
	url:CCF.structuralQC.PROCESSING_STATUS_URL,
	cache: false,
	async: true,
	context: this
    }).done( function(data, textStatus, jqXHR) {
    	$("#statusModal").width("950px");
    	$("#statusModal").height("475px");
    	$("#strucqc-content").css({ 'overflow':'auto', 'font-size':'small' });
    	CCF.structuralQC.modalMessage(jqXHR.responseText);
    }).fail( function(data, textStatus, jqXHR) {
	CCF.structuralQC.modalError("ERROR:  Could not obtain processing status (RESULT=" + textStatus + ").");
    });

}


