
//# sourceURL=structuralQC/structuralQcConfig.js

if (typeof CCF === 'undefined') {
	CCF = {};
}
if (typeof CCF.structuralQcConfig === 'undefined') {
	CCF.structuralQcConfig = {
	}
}


CCF.structuralQcConfig.checkPcpSetup = function() {

			$.ajax({
				type: "GET",
				url:serverRoot+'/xapi/pipelineControlPanelConfig/' + XNAT.data.projectID + '/settings',
				cache: false,
				async: false,
				context: this,
				dataType: 'json'
			}).done( function(data, textStatus, jqXHR) {
				CCF.structuralQcConfig.pcpsettings = data;
			});
			CCF.structuralQcConfig.pcpsettings.hasStructuralQcPipeline = false;
			for (var i=0; i<CCF.pcpconfig.currentData.length; i++) {
				var p = CCF.pcpconfig.currentData[i];
				if (p.pipeline == "StructuralQC") {
					CCF.structuralQcConfig.pcpsettings.hasStructuralQcPipeline = true;
				} 
			}
			if (CCF.structuralQcConfig.pcpsettings.hasStructuralQcPipeline) {
				return;
			}
			xmodal.confirm({
				title:  "Setup Pipeline Control Panel for Structural QC",
				content:  "A <em>Pipeline Control Panel</em> pipeline is available to manage Structural QC processing.  A check shows that may not be installed." +
						"<br><br><b>Do you wish to configure the pipeline control panel to add setup for Structural QC now?</b>",
				width: 500,
				height: 250,
				okLabel:  "OK",
				okClose:  true,
				okAction:  function() {
					CCF.structuralQcConfig.pcpSetup();
				}
			});

}


CCF.structuralQcConfig.pcpSetup = function() {
	if (!CCF.structuralQcConfig.pcpsettings.hasStructuralQcPipeline) {
		var tp = '{"pipeline":"StructuralQC","description":"Structural QC Pipeline","updateFrequency":"Daily","selector":"org.nrg.ccf.pcpcomponents.selectors.AllMRSessionsPipelineSelector","submitter":"org.nrg.structuralqc.components.pcp.StructuralQcSubmitter","statusUpdater":"org.nrg.ccf.pcpcomponents.statusupdaters.SimpleStatusUpdater","prereqChecker":"org.nrg.structuralqc.components.pcp.StructuralQcPrereqChecker","validator":"org.nrg.structuralqc.components.pcp.StructuralQcValidator","execManager":"org.nrg.ccf.pcpcomponents.execmanagers.SimpleExecManager","configurables":{}}';
		var parsedTP = JSON.parse(tp);
		if (parsedTP.pipeline !== 'undefined') {
			CCF.pcpconfig.currentData.push(parsedTP);
			CCF.pcpconfig.prevData = CCF.pcpconfig.currentData;
			CCF.pcpconfig.postDataUpdates('saved',false); 
			CCF.pcpconfig.populatePipelinesTable('#pipelines-list-container');
			CCF.pcpconfig.refreshPipelineCache('StructuralQC');
		}
	}
}


$(document).ready(function() {
	$("#structural-qc-project-settings-panel").find("button.save").mousedown(function() {
		if (typeof CCF.pcpconfig === 'undefined' || typeof CCF.pcpconfig.currentData === 'undefined') {
			return;
		}
		if ($("input[name='structuralQcEnabled']").val().toString().toLowerCase() == "true") {
			setTimeout(function() {
				CCF.structuralQcConfig.checkPcpSetup();
			},1000);
		}
	});
});


